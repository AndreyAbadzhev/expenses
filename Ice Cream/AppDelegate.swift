//
//  AppDelegate.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 05/11/2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    let rootManager = RootManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        rootManager.start()
        
        return true
    }
}

