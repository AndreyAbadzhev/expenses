//
//  TableView.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
import UIKit
extension UITableView {
    
    func register<T: UITableViewCell>(_ type: T.Type) {
        self.register(UINib(nibName: String(describing: type),bundle: nil),
                      forCellReuseIdentifier: String(describing: type))
    }
    
    func deque<T: UITableViewCell>(_ type: T.Type) -> T? {
        return self.dequeueReusableCell(withIdentifier: String(describing: type)) as? T
    }
    
    func reloadData(completion: @escaping() -> ()) {
        UIView.animate(withDuration: 0, animations: {
                    self.reloadData()
                }, completion: { _ in
                    completion()
                })
    }
    
    func reloadDataWithAnimation(completion: @escaping() -> ()) {
//        let range = NSMakeRange(0, self.numberOfSections)
//        let sections = NSIndexSet(indexesIn: range)
        UIView.animate(withDuration: 0, animations: {
//            self.reloadSections(sections as IndexSet, with: .automatic)
            self.reloadData()
        }, completion: { _ in
            completion()
        })
        
    }
    
}
