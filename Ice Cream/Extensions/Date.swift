//
//  Date.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 01.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
extension Date {
    
    var monthAndYearString: String {
        let calendar = Calendar.current
        switch true {
        case calendar.isDate(self, equalTo: Date(), toGranularity: .year): return monthString
        default: return "\(monthString) \(calendar.component(.year, from: self))"
        }
    }
    
    var monthString: String {
        let calendar = Calendar.current
        switch calendar.component(.month, from: self) {
        case 1: return "Январь"
        case 2: return "Февраль"
        case 3: return "Март"
        case 4: return "Апрель"
        case 5: return "Май"
        case 6: return "Июнь"
        case 7: return "Июль"
        case 8: return "Август"
        case 9: return "Сентябрь"
        case 10: return "Октябрь"
        case 11: return "Ноябрь"
        case 12: return "Декабрь"
        default: return ""
        }
    }
    
    var dayAndWeekdayString: String {
        let calendar = Calendar.current
        switch true {
        case calendar.isDateInToday(self): return "Сегодня"
        case calendar.isDateInYesterday(self): return "Вчера"
        case calendar.isDate(self, equalTo: Date(), toGranularity: .weekOfMonth): return weekdayString
        default: return "\(weekdayString) \(calendar.component(.day, from: self))"
        }
    }
    
    var weekdayString: String {
        let calendar = Calendar.current
        switch calendar.component(.weekday, from: self) {
        case 1: return "Воскресенье"
        case 2: return "Понедельник"
        case 3: return "Вторник"
        case 4: return "Среда"
        case 5: return "Четверг"
        case 6: return "Пятница"
        case 7: return "Суббота"
        default: return ""
        }
    }
    
    func monthIsCurrentMonth() -> Bool {
        let now = Date()
        let calendar = Calendar.current
        
        return calendar.isDate(self, equalTo: now, toGranularity: .year) && calendar.isDate(self, equalTo: now, toGranularity: .month)
    }
    
    func dayIsCurrentDay() -> Bool {
        let now = Date()
        let calendar = Calendar.current
        
        return calendar.isDate(self, equalTo: now, toGranularity: .day)
    }
    
    func monthIsEqualToMonth(date: Date) -> Bool {
        let calendar = Calendar.current
        
        return calendar.isDate(self, equalTo: date, toGranularity: .year) && calendar.isDate(self, equalTo: date, toGranularity: .month)
    }
    
    func dayIsEqualToDay(date: Date) -> Bool {
        let calendar = Calendar.current
        
        return calendar.isDate(self, equalTo: date, toGranularity: .day)
    }
    
    func dateInCurrentYear() -> Bool {
        let now = Date()
        let calendar = Calendar.current
        
        return calendar.isDate(self, equalTo: now, toGranularity: .year)
    }
    
    func dateInNextYear() -> Bool {
        let now = Date()
        let calendar = Calendar.current
        var dateComponents = calendar.dateComponents([.hour, .minute, .second, .day, .month, .year], from: now)
        dateComponents.year = dateComponents.year! + 1
        let nextYear = calendar.date(from: dateComponents) ?? Date()
        
        return calendar.isDate(self, equalTo: nextYear, toGranularity: .year)
    }
}
