//
//  Color.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 28.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit
extension UIColor {
    
    static var unusedDarkTwo: UIColor {
        return UIColor(red: 239/255, green: 167/255, blue: 69/255, alpha: 1)
    }
    
    static var unusedLightTwo: UIColor {
        return UIColor(red: 250/255, green: 225/255, blue: 184/255, alpha: 1)
    }
    
    static var unusedDarkOne: UIColor {
        return UIColor(red: 245/255, green: 231/255, blue: 112/255, alpha: 1)
    }
    
    static var unusedLightOne: UIColor {
        return UIColor(red: 254/255, green: 250/255, blue: 201/255, alpha: 1)
    }
    
    
    static var utilitiesDark: UIColor {
        return UIColor(red: 163/255, green: 200/255, blue: 111/255, alpha: 1)
    }
    
    static var utilitiesLight: UIColor {
        return UIColor(red: 224/255, green: 236/255, blue: 204/255, alpha: 1)
    }
    
    static var accumulationDark: UIColor {
        return UIColor(red: 80/255, green: 160/255, blue: 150/255, alpha: 1)
    }
    
    static var accumulationLight: UIColor {
        return UIColor(red: 187/255, green: 222/255, blue: 220/255, alpha: 1)
    }
    
    static var creditsDark: UIColor {
        return UIColor(red: 93/255, green: 189/255, blue: 209/255, alpha: 1)
    }
    
    static var creditsLight: UIColor {
        return UIColor(red: 190/255, green: 235/255, blue: 241/255, alpha: 1)
    }
    
    static var transportDark: UIColor {
        return UIColor(red: 92/255, green: 158/255, blue: 235/255, alpha: 1)
    }
    
    static var transportLight: UIColor {
        return UIColor(red: 194/255, green: 220/255, blue: 248/255, alpha: 1)
    }
    
    static var miscDark: UIColor {
        return UIColor(red: 159/255, green: 77/255, blue: 181/255, alpha: 1)
    }
    
    static var miscLight: UIColor {
        return UIColor(red: 220/255, green: 190/255, blue: 230/255, alpha: 1)
    }
    
    static var petsDark: UIColor {
        return UIColor(red: 93/255, green: 104/255, blue: 181/255, alpha: 1)
    }
    
    static var petsLight: UIColor {
        return UIColor(red: 199/255, green: 202/255, blue: 232/255, alpha: 1)
    }
    
    static var foodDark: UIColor {
        return UIColor(red: 209/255, green: 90/255, blue: 81/255, alpha: 1)
    }
    
    static var foodLight: UIColor {
        return UIColor(red: 245/255, green: 204/255, blue: 206/255, alpha: 1.0)
    }
    
    static var textDark: UIColor {
        return UIColor(red: 118/255, green: 126/255, blue: 151/255, alpha: 1)
    }
    
    static var textLight: UIColor {
        return UIColor(red: 193/255, green: 193/255, blue: 199/255, alpha: 1)
//        return UIColor(red: 200/255, green: 183/255, blue: 171/255, alpha: 1)
    }
    
    static var orangeLight: UIColor {
        return UIColor(red: 238/255, green: 172/255, blue: 71/255, alpha: 1)
    }
    
    static var blackVeryLight: UIColor {
        return UIColor(red: 83/255, green: 89/255, blue: 101/255, alpha: 1)
    }
    
    static var blackLight: UIColor {
        return UIColor(red: 39/255, green: 46/255, blue: 71/255, alpha: 1)
    }
    
    static var blackDark: UIColor {
        return UIColor(red: 29/255, green: 34/255, blue: 57/255, alpha: 1)
    }
    
    static var gradientDark: UIColor {
//        return UIColor(red: 68/255, green: 46/255, blue: 78/255, alpha: 1)
//        return UIColor(red: 224/255, green: 107/255, blue: 132/255, alpha: 1)
        return UIColor(red: 22/255, green: 26/255, blue: 46/255, alpha: 1)
    }
    
    static var gradientLight: UIColor {
//        return UIColor(red: 152/255, green: 94/255, blue: 119/255, alpha: 1)
//        return UIColor(red: 242/255, green: 181/255, blue: 109/255, alpha: 1)
        return UIColor(red: 22/255, green: 26/255, blue: 46/255, alpha: 1)
    }
    
    static var redLight: UIColor {
//        return UIColor(red: 223/255, green: 149/255, blue: 140/255, alpha: 1)
//        return UIColor(red: 219/255, green: 114/255, blue: 93/255, alpha: 1)
        return UIColor(red: 224/255, green: 107/255, blue: 132/255, alpha: 1)
    }
    
    static var redDark: UIColor {
//        return UIColor(red: 179/255, green: 95/255, blue: 119/255, alpha: 1)
        return UIColor(red: 170/255, green: 60/255, blue: 54/255, alpha: 1)
    }
    
    static var redWarning: UIColor {
        return UIColor(red: 255/255, green: 82/255, blue: 82/255, alpha: 1)
    }
    
//    FF5252
    
    static var yellowLight: UIColor {
//        return UIColor(red: 246/255, green: 198/255, blue: 147/255, alpha: 1)
        return UIColor(red: 242/255, green: 181/255, blue: 109/255, alpha: 1)
    }
    
//    static var purpleLight: UIColor {
//        return UIColor(red: 179/255, green: 136/255, blue: 255/255, alpha: 1)
//    }
    
    static var greenLight: UIColor {
        return UIColor(red: 197/255, green: 225/255, blue: 165/255, alpha: 1) // C5E1A5
    }
    
    static var purpleDark: UIColor {
        return UIColor(red: 86/255, green: 46/255, blue: 69/255, alpha: 1)
    }

    
}
