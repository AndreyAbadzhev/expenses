//
//  Button.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 11.01.2020.
//  Copyright © 2020 Private. All rights reserved.
//

import UIKit
extension UIButton {
    func makeCardWithBorders() {
        self.clipsToBounds = true
        self.layer.cornerRadius = 2

        self.setTitleColor(.orangeLight, for: .normal)
        
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.orangeLight.cgColor
    }
}
