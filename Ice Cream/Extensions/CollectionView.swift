//
//  CollectionView.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 13.01.2020.
//  Copyright © 2020 Private. All rights reserved.
//

import UIKit
extension UICollectionView {

    func register<T: UICollectionViewCell>(_ type: T.Type) {
        self.register(UINib(nibName: String(describing: type),bundle: nil),
                      forCellWithReuseIdentifier: String(describing: type))
    }

    func deque<T: UICollectionViewCell>(_ type: T.Type, for indexPath: IndexPath) -> T? {
        return self.dequeueReusableCell(withReuseIdentifier: String(describing: type), for: indexPath) as? T
    }
}
