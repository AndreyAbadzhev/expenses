//
//  TextFiled.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 28.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit
extension UITextField {
    
    func configureForDatePicking(datePicker: UIDatePicker, target: UIViewController, doneSelector: Selector, cancelSelector: Selector) {
        datePicker.datePickerMode = .date
        self.keyboardAppearance = .dark

        let toolbar = UIToolbar()
        toolbar.tintColor = .orangeLight
        toolbar.sizeToFit()

           
        let doneButton = UIBarButtonItem(title: "Готово", style: UIBarButtonItem.Style.done, target: target, action: doneSelector)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Отмена", style: UIBarButtonItem.Style.plain, target: target, action: cancelSelector)
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)

        self.inputAccessoryView = toolbar
        self.inputView = datePicker
    }
    
    func configureForPricePicking() {
        self.keyboardAppearance = .dark
        
        let toolbar = UIToolbar()
        toolbar.tintColor = .orangeLight
        toolbar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Готово", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.superview?.endEditing))
        toolbar.setItems([spaceButton, spaceButton, doneButton], animated: false)
        self.inputAccessoryView = toolbar
    }
    
}
