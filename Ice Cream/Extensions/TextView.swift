//
//  TextView.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 11.01.2020.
//  Copyright © 2020 Private. All rights reserved.
//

import UIKit
extension UITextView {
    
    func configureForCommentEditing() {
        let toolbar = UIToolbar()
        toolbar.tintColor = .orangeLight
        toolbar.sizeToFit()
        
        self.keyboardAppearance = .dark
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Готово", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.superview?.endEditing))
        toolbar.setItems([spaceButton, spaceButton, doneButton], animated: false)
        self.inputAccessoryView = toolbar
    }
    
}
