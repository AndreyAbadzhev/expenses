//
//  Array.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
extension Array {
    func getElement(for index: Int) -> Element? {
        return self.count > index ? self[index] : nil
    }
    
    func getLastElement() -> Element? {
        return self.count > 0 ? self.last : nil
    }
}
