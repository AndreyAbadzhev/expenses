//
//  View.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit
extension UIView {
    
    static func loadFromNib<T: UIView>(_ type: T.Type) -> T? {
        return UINib(nibName: String(describing: type), bundle: nil).instantiate(withOwner: nil, options: nil).getElement(for: 0) as? T
    }
    
    func addGradientBackground() {
        let gradient = CAGradientLayer()
        gradient.locations = [0, 1]
        gradient.colors = [UIColor.gradientLight.cgColor, UIColor.gradientDark.cgColor]
        gradient.frame = self.bounds
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func addShadow() {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 6
        self.layer.masksToBounds = false
    }
    
    // На весь экран (скрывает элементы перед таббаром)
    func addMask() {
        let gradientMaskLayer: CAGradientLayer = CAGradientLayer()
        gradientMaskLayer.frame = self.bounds
        gradientMaskLayer.colors = [UIColor.black.cgColor,
                                    UIColor.black.cgColor,
                                    UIColor.clear.cgColor,
                                    UIColor.clear.cgColor]
        gradientMaskLayer.locations = [0.0, 0.5, 0.8, 1]
        self.layer.mask = gradientMaskLayer
    }
    
    func addGradientHorizontal(mainColor: UIColor, additionalColor: UIColor, visibleWidth: CGFloat) {
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [mainColor.cgColor,
                                mainColor.cgColor,
                                additionalColor.cgColor,
                                additionalColor.cgColor]
        
        let visibleWidthProportional: Double = Double(visibleWidth / UIScreen.main.bounds.width * 10) / 10
        let visibleWidthProportionalTail: Double = 0.15
        
        gradientLayer.locations = [0.0, NSNumber(floatLiteral: Double(visibleWidthProportional)), NSNumber(floatLiteral: Double(visibleWidthProportional + visibleWidthProportionalTail)), 1]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradientLayer.masksToBounds = true
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func makeCard(filledWith color: UIColor) {
        self.clipsToBounds = true
        self.layer.cornerRadius = 2
        self.backgroundColor = color
    }
    
    func animatedShowWithDelay(delay: Double) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
            UIView.animate(withDuration: 0.3, animations: {
                self.alpha = 1
            })
        })
    }
    
    func animatedShow(completion: @escaping() -> ()) {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 1
        }, completion: { _ in
            completion()
        })
    }
    
    func animatedHide(completion: @escaping() -> ()) {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        }, completion: { _ in
            completion()
        })
    }
    
    func hide() {
        self.alpha = 0
    }
    
    func compress() {
        self.transform = self.transform.scaledBy(x: 0.001, y: 0.001)
    }
    
    func animateCompress(completion: @escaping() -> ()) {
        UIView.animate(withDuration: 0.3, animations: {
            self.transform = self.transform.scaledBy(x: 0.001, y: 0.001)
        }, completion: { _ in
            completion()
        })
    }
    
    func animateDecompress(completion: @escaping() -> ()) {
        UIView.animate(withDuration: 0.3, animations: {
            self.transform = self.transform.scaledBy(x: 1000, y: 1000)
        }, completion: { _ in
            completion()
        })
    }
    
    func animateDecompressWithDelay(delay: Double, completion: @escaping() -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
            UIView.animate(withDuration: 0.3, animations: {
                self.transform = self.transform.scaledBy(x: 1000, y: 1000)
            }, completion: { _ in
                completion()
            })
        })
    }
}
