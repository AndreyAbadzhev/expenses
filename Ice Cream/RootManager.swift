//
//  RootManager.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 22.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit
class RootManager {
    
    let window: UIWindow = UIWindow(frame: UIScreen.main.bounds)
    let tabbarController: TabbarController = TabbarController()
    
    func start() {
        setupAppearence()
        window.makeKeyAndVisible()
        window.rootViewController = tabbarController
    }
    
    func setupAppearence() {
        UINavigationBar.appearance().isOpaque = true
        UINavigationBar.appearance().tintColor = .orangeLight
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: .light),
                                                            NSAttributedString.Key.foregroundColor: UIColor.textLight]
        
        UITabBar.appearance().backgroundImage = UIImage()
        UITabBar.appearance().clipsToBounds = true
        UITabBar.appearance().unselectedItemTintColor = .textDark
        UITabBar.appearance().tintColor = .orangeLight
        
//        if #available(iOS 13, *) {
//            let statusBar = UIView(frame: (UIApplication.shared.windows[0].windowScene?.statusBarManager?.statusBarFrame)!)
//            statusBar.backgroundColor = .orangeLight
//            UIApplication.shared.windows[0].addSubview(statusBar)
//        } else {
//            let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//            if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
//                statusBar.backgroundColor = .orangeLight
//            }
//            UIApplication.shared.statusBarStyle = .lightContent
//        }
    }
}
