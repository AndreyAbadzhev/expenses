//
//  extensions.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 07/11/2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation

extension Array {
    func getElement(index: Int) -> Element? {
        return self.count > index ? self[index] : nil
    }
}
