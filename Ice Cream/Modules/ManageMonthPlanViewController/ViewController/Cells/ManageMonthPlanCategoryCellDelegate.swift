//
//  ManageMonthPlanCategoryCellDelegate.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 21.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol ManageMonthPlanCategoryDelegate {
    func monthPlanCategoryAmountChanged(for indexPath: IndexPath, newAmount: Int)
}
