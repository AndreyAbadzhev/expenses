//
//  ManageMonthPlanCategoryCell.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 18.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

class ManageMonthPlanCategoryCell: UITableViewCell {

    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var totalAmountWithPlannedExpensesLabel: UILabel!
    
    var delegate: ManageMonthPlanCategoryDelegate?
    var indexPath: IndexPath = IndexPath(row: 0, section: 0)
    
    var plannedCategoryAmount: Int = 0
    var plannedExpensesAmount: Int = 0
    
    func fillCell(withDelegate delegate: ManageMonthPlanCategoryDelegate, plannedExpenseByCategory: PlannedExpensesByCategory, plannedExpensesMatchingCategory: [Expense], for indexPath: IndexPath) {
        self.delegate = delegate
        self.indexPath = indexPath
        self.plannedCategoryAmount = plannedExpenseByCategory.plannedAmount
        self.plannedExpensesAmount = plannedExpensesMatchingCategory.reduce(0, { $0 + $1.amount })
        self.categoryNameLabel.text = plannedExpenseByCategory.expenseCategory.rawValue
        
        self.updateViews()
    }
    
    func updateViews() {
        self.amountTextField.text = String(plannedCategoryAmount)
        
        if plannedExpensesAmount != 0 {
            let totalPlannedAmount = plannedCategoryAmount + plannedExpensesAmount
            totalAmountWithPlannedExpensesLabel.text = "С учетом запланированных покупок - \(totalPlannedAmount)"
        } else {
            totalAmountWithPlannedExpensesLabel.text = "Запланированных покупок в этой категории нет"
        }
    }
}

extension ManageMonthPlanCategoryCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = textField.text?.replacingCharacters(in: Range(range, in : textField.text!)!, with: string)
        textField.text = newString

        delegate?.monthPlanCategoryAmountChanged(for: indexPath, newAmount: Int(newString ?? "") ?? 0)
        self.plannedCategoryAmount = Int(newString ?? "") ?? 0
        updateViews()
        
        return false
    }
}
