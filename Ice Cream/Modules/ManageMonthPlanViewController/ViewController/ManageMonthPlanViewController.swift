//
//  ManageMonthPlanViewController.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 18.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

class ManageMonthPlanViewController: UIViewController, ManageMonthPlanViewInput {

    @IBOutlet weak var tableView: UITableView!
    
    var output: ManageMonthPlanViewOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Manage"
        setupInitialState()
        output?.viewIsReady()
    }
    
    func setupInitialState() {
        setupNavigationBar()
        setupTableView()
    }
    
    func setupNavigationBar() {
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneBarButtonDidTap))
        navigationItem.rightBarButtonItem = doneBarButton
    }
    
    func setupTableView() {
        tableView.register(ManageMonthPlanCategoryCell.self)
        tableView.register(ExpenseCell.self)
        tableView.sectionHeaderHeight = 44
    }
    
    func updateState() {
        tableView.reloadData()
    }
    
    func updateCell(at indexPath: IndexPath) {
        self.tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func insertRaw(at indexPath: IndexPath) {
        tableView.insertRows(at: [indexPath], with: .automatic)
        guard let cell = self.tableView.cellForRow(at: indexPath) as? ExpenseCell else { return }
        cell.creationFlowStart()
    }
    
    func deleteRaw(at indexPath: IndexPath) {
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    
    func updateFirstSection() {
        tableView.reloadSections(IndexSet(arrayLiteral: 0), with: .none)
    }
    
    @objc func doneBarButtonDidTap() {
        output?.doneBarButtonDidTap()
    }
}

extension ManageMonthPlanViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return output?.sections.getElement(for: section)?.header ?? "Не указано"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output?.sections.getElement(for: section)?.cells.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = output?.sections.getElement(for: indexPath.section)?.cells.getElement(for: indexPath.row) else { return UITableViewCell() }
        
        switch model {
        case .plannedExpensesByCategory(let plannedExpensesByCategory):
            guard let cell = tableView.deque(ManageMonthPlanCategoryCell.self) else { return UITableViewCell() }
            cell.fillCell(withDelegate: self, plannedExpenseByCategory: plannedExpensesByCategory.plannedExpensesByCategory, plannedExpensesMatchingCategory: plannedExpensesByCategory.plannedExpensesMatchingCategory, for: indexPath)
            return cell
        case .plannedExpense(let plannedExpense):
            guard let cell = tableView.deque(ExpenseCell.self) else { return UITableViewCell() }
            cell.fillCell(withDelegate: self, andWithExpense: plannedExpense, using: .updateMode)
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return output?.sections.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 1 ? 66 : 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 1 {
            let sectionsFooterView = UIView.loadFromNib(AddNewRowForExpenseFooter.self)
            sectionsFooterView?.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 66)
            sectionsFooterView?.fillCell(delegate: self, withMode: .planned)
            return sectionsFooterView
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete && indexPath.section == 1 { output?.deleteExpenseDidTapped(for: indexPath) }
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
       
        let deleteButton = UIContextualAction(style: .destructive, title: "Удалить\nпокупку", handler: {_,_,_ in
            self.tableView.dataSource?.tableView!(self.tableView, commit: .delete, forRowAt: indexPath)
            return
        })
        deleteButton.backgroundColor = .gradientLight

        let swipeActionsConfiguration = UISwipeActionsConfiguration(actions: [deleteButton])

        return swipeActionsConfiguration
    }
}

extension ManageMonthPlanViewController: AddExpeseCellDelegate {
    func categoryButtonDidTap(for expenseWithId: Double) {
        output?.categoryButtonDidTap(for: expenseWithId)
    }
    
    func expenseAmountHasChanged(for expenseWithId: Double, newAmount: Int) {
        output?.expenseAmountHasChanged(for: expenseWithId, newAmount: newAmount)
    }
    
    func addNewRowForExpenseButtonDidTap() {
        output?.addNewRowForExpenseButtonDidTap()
    }
    
    func commentHasChanged(for expenseWithId: Double, newComment: String) {
        output?.commentHasChanged(for: expenseWithId, newComment: newComment)
        tableView.beginUpdates(); tableView.endUpdates()
    }
    
    func commentExpandButtonDidTap() {
        tableView.beginUpdates(); tableView.endUpdates()
    }
}

extension ManageMonthPlanViewController: ManageMonthPlanCategoryDelegate {
    func monthPlanCategoryAmountChanged(for indexPath: IndexPath, newAmount: Int) {
        output?.monthPlanCategoryAmountChanged(for: indexPath, newAmount: newAmount)
    }
}

