//
//  ManageMonthPlanViewInput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 18.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol ManageMonthPlanViewInput: class {
    func updateState()
    func updateFirstSection()
    func updateCell(at indexPath: IndexPath)
    
    func insertRaw(at indexPath: IndexPath)
    func deleteRaw(at indexPath: IndexPath)
}
