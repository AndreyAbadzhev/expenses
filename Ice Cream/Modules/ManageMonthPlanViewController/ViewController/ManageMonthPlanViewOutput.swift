//
//  ManageMonthPlanViewOutput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 18.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol ManageMonthPlanViewOutput {
    var sections: [ManageMonthPlanSection] { get }
    
    func viewIsReady()
    
    func monthPlanCategoryAmountChanged(for indexPath: IndexPath, newAmount: Int)
    
    func categoryButtonDidTap(for expenseWithId: Double)
    
    func expenseAmountHasChanged(for expenseWithId: Double, newAmount: Int)
    func commentHasChanged(for expenseWithId: Double, newComment: String)
    func addNewRowForExpenseButtonDidTap()
    
    func deleteExpenseDidTapped(for indexPath: IndexPath)
    
    func doneBarButtonDidTap()
}
