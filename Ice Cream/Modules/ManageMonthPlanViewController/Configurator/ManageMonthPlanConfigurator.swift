//
//  ManageMonthPlanConfigurator.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 18.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class ManageMonthPlanConfigurator {
    
    func configure(with viewController: ManageMonthPlanViewController, realmManager: RealmManager, month: Month, completion: @escaping() -> ()) {
        let presenter = ManageMonthPlanPresenter()
        let interactor = ManageMonthPlanInteractor(realmManager: realmManager, month: month)
        let router = ManageMonthPlanRouter(with: viewController, completion: completion)
        
        viewController.output = presenter
        
        presenter.viewController = viewController
        presenter.interactor = interactor
        presenter.router = router
    }
    
}
