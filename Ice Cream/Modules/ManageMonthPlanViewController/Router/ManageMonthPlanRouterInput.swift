//
//  ManageMonthPlanRouterInput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 18.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol ManageMonthPlanRouterInput {
    func showCategoriesListView(categorySelectedCompletion: @escaping (ExpenseCategory) -> ())
    func changeMonthPlanCompletion()
}
