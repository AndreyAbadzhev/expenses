//
//  ManageMonthPlanRouter.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 18.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
import UIKit
class ManageMonthPlanRouter: ManageMonthPlanRouterInput {
    
    weak var viewController: ManageMonthPlanViewController?
    var completion: (() -> ())?
    
    init(with viewController: ManageMonthPlanViewController, completion: @escaping() -> ()) {
        self.viewController = viewController
        self.completion = completion
    }
    
    func showCategoriesListView(categorySelectedCompletion: @escaping (ExpenseCategory) -> ()) {

        guard let categoriesListView = UIView.loadFromNib(ListSelectCustomView.self) else { return }
        categoriesListView.categorySelectedCompletion = categorySelectedCompletion
        categoriesListView.frame = CGRect(x: 0,
                                           y: 0,
                                           width: UIScreen.main.bounds.width,
                                           height:  UIScreen.main.bounds.height)

        viewController?.view.addSubview(categoriesListView)
    }
    
    func changeMonthPlanCompletion() {
        completion?()
    }
    
}
