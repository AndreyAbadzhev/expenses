//
//  ManageMonthPlanInteractorInput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 18.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol ManageMonthPlanInteractorInput {

    var plannedExpensesByCategory: [PlannedExpensesByCategory] { get }
    var plannedExpenses: [Expense] { get }
    
    func prepareData()
    
    func removePlannedExpensesByCategory()
    func refillPlannedExpensesByCategory()
    
    func monthPlanCategoryAmountChanged(for category: ExpenseCategory, newAmount: Int)
    
    func categoryHasChanged(for expenseWithId: Double, newCategory: ExpenseCategory)
    func expenseAmountHasChanged(for expenseWithId: Double, newAmount: Int)
    func commentHasChanged(for expenseWithId: Double, newComment: String)
    func addNewExpese()
    
    func deleteExpense(for expenseWithId: Double)
    
    func saveMonth(completion: @escaping() -> ())
}
