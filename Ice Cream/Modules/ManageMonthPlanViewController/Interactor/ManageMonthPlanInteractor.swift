//
//  ManageMonthPlanInteractor.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 18.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class ManageMonthPlanInteractor: ManageMonthPlanInteractorInput {
    
    let realmManager: RealmManager
    var month: Month
    
    var plannedExpensesByCategory: [PlannedExpensesByCategory] = []
    var plannedExpenses: [Expense] = []
    
    init(realmManager: RealmManager, month: Month) {
        self.realmManager = realmManager
        self.month = month
    }

    func prepareData() {
        self.plannedExpensesByCategory = month.plannedExpensesByCategory
        self.plannedExpenses = month.plannedExpenses
    }
    
    func removePlannedExpensesByCategory() {
        plannedExpensesByCategory.removeAll()
    }
    
    func refillPlannedExpensesByCategory() {
        plannedExpensesByCategory = month.plannedExpensesByCategory
    }
    
    func monthPlanCategoryAmountChanged(for category: ExpenseCategory, newAmount: Int) {
        if let index = plannedExpensesByCategory.firstIndex(where: { $0.expenseCategory == category }) {
            plannedExpensesByCategory[index].plannedAmount = newAmount
        } else {
            plannedExpensesByCategory.append(PlannedExpensesByCategory(expenseCategory: category, plannedAmount: newAmount))
        }
    }
    
    func categoryHasChanged(for expenseWithId: Double, newCategory: ExpenseCategory) {
        if let index = plannedExpenses.firstIndex(where: { $0.unixId == expenseWithId}) {
            plannedExpenses[index].expenseCategory = newCategory
        }
    }
    
    func expenseAmountHasChanged(for expenseWithId: Double, newAmount: Int) {
        if let index = plannedExpenses.firstIndex(where: { $0.unixId == expenseWithId}) {
            plannedExpenses[index].amount = newAmount
        }
    }
    
    func commentHasChanged(for expenseWithId: Double, newComment: String) {
        if let index = plannedExpenses.firstIndex(where: { $0.unixId == expenseWithId}) {
            plannedExpenses[index].comment = newComment
        }
    }
    
    func addNewExpese() {
        let expense = Expense(unixId: Date().timeIntervalSince1970, amount: 0, expenseCategory: .misc, comment: "Без комментария", planningStaus: .planned)
        plannedExpenses.insert(expense, at: 0)
    }
    
    func deleteExpense(for expenseWithId: Double) {
        plannedExpenses.removeAll(where: { $0.unixId == expenseWithId })
    }
    
    func saveMonth(completion: @escaping() -> ()) {
        month.plannedExpensesByCategory = plannedExpensesByCategory.filter { $0.plannedAmount > 0 }
        month.plannedExpenses = plannedExpenses.filter { $0.amount > 0 }
        
        month.prepareToPutToRealm()
        realmManager.write(plainObject: month, completion: completion)
    }
}
