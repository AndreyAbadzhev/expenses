//
//  ManageMonthPlan.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 18.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class ManageMonthPlanPresenter: ManageMonthPlanViewOutput {
    
    weak var viewController: ManageMonthPlanViewInput?
    var interactor: ManageMonthPlanInteractorInput?
    var router: ManageMonthPlanRouterInput?
    
    var sections: [ManageMonthPlanSection] = []
    
    func viewIsReady() {
        interactor?.prepareData()
        updateSections()
        viewController?.updateState()
    }
    
    func updateSections() {
        sections.removeAll()
        
        var categoriesCells: [ManageMonthPlanCellType] = []
        
        fullCategoriesArray.forEach {
            let category = $0
            let plannedCategory = interactor?.plannedExpensesByCategory.first(where: { $0.expenseCategory == category })
            let plannedExpensesMatchingCategory = interactor?.plannedExpenses.filter { $0.expenseCategory == category }
            
            let cell = ManageMonthPlanCellType.plannedExpensesByCategory(plannedExpensesByCategory: PlannedExpensesByCategory(expenseCategory: category, plannedAmount: plannedCategory?.plannedAmount ?? 0), plannedExpensesMatchingCategory: plannedExpensesMatchingCategory ?? [])
            categoriesCells.append(cell)
        }
        
        let header = "Бюджет по категорям"
        sections.append(ManageMonthPlanSection(header: header, cells: categoriesCells))
        
        
        if let plannedExpenses = interactor?.plannedExpenses {
            let header = "Запланированные покупки"
            let cells: [ManageMonthPlanCellType] = plannedExpenses.map { .plannedExpense(plannedExpense: $0) }
            sections.append(ManageMonthPlanSection(header: header, cells: cells))
        }
    }
    
    func monthPlanCategoryAmountChanged(for indexPath: IndexPath, newAmount: Int) {
        guard let category = fullCategoriesArray.getElement(for: indexPath.row) else { return }
        interactor?.monthPlanCategoryAmountChanged(for: category, newAmount: newAmount)
    }
    
    func categoryButtonDidTap(for expenseWithId: Double) {
        router?.showCategoriesListView(categorySelectedCompletion: { [weak self] selectedCategory in
            self?.interactor?.categoryHasChanged(for: expenseWithId, newCategory: selectedCategory)
            self?.updateSections()
            self?.viewController?.updateFirstSection()
            
            for (index, cell) in (self?.sections.getElement(for: 1)?.cells ?? []).enumerated() {
                if case let .plannedExpense(plannedExpense) = cell {
                    if plannedExpense.unixId == expenseWithId {
                        self?.viewController?.updateCell(at: IndexPath(row: index, section: 1))
                    }
                }
            }
            
        })
    }
    
    func expenseAmountHasChanged(for expenseWithId: Double, newAmount: Int) {
        interactor?.expenseAmountHasChanged(for: expenseWithId, newAmount: newAmount)
        updateSections()
        viewController?.updateFirstSection()
    }
    
    func commentHasChanged(for expenseWithId: Double, newComment: String) {
        interactor?.commentHasChanged(for: expenseWithId, newComment: newComment)
    }
    
    func addNewRowForExpenseButtonDidTap() {
        interactor?.addNewExpese()
        updateSections()
        viewController?.insertRaw(at: IndexPath(row: 0, section: 1))
    }
    
    func deleteExpenseDidTapped(for indexPath: IndexPath) {
        guard let cell = sections.getElement(for: indexPath.section)?.cells[indexPath.row] else { return }
        if case let .plannedExpense(plannedExpense) = cell {
            interactor?.deleteExpense(for: plannedExpense.unixId)
            updateSections()
            viewController?.deleteRaw(at: indexPath)
        }
    }
    
    func doneBarButtonDidTap() {
        interactor?.saveMonth(completion: { [weak self] in
            self?.router?.changeMonthPlanCompletion()
        })
    }
}

struct ManageMonthPlanSection {
    let header: String
    let cells: [ManageMonthPlanCellType]
}

enum ManageMonthPlanCellType {
    case plannedExpensesByCategory(plannedExpensesByCategory: PlannedExpensesByCategory, plannedExpensesMatchingCategory: [Expense])
    case plannedExpense(plannedExpense: Expense)
}
