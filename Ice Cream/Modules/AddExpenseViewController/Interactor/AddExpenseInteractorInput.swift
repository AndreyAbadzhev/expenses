//
//  AddExpenseInteractorInput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol AddExpenseInteractorInput {
    
    var chosenDate: Date { get set }

    var newExpenses: [Expense] { get }
    var plannedExpenses: [Expense] { get }
    
    func getMonth(completion: @escaping() -> ())
    
    func categoryHasChanged(for expenseWithId: Double, newCategory: ExpenseCategory)
    func expenseAmountHasChanged(for expenseWithId: Double, newAmount: Int)
    func commentHasChanged(for expenseWithId: Double, newComment: String)
    func addNewExpese()
    func writeNewExpensesToRealm(completion: @escaping() -> ())

    func removeExpense(for expenseWithId: Double)
    
    func plannedExpenseStatusChanged(for expenseWithId: Double, newStatus: ExpensePlanningStatus)
}
