//
//  AddExpenseInteractor.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class AddExpenseInteractor: AddExpenseInteractorInput {
    
    var realmManager: RealmManager
    
    var chosenDate: Date = Date()
    var month: Month?
    
    var newExpenses: [Expense] = []
    var plannedExpenses: [Expense] = []
    
    init(realmManager: RealmManager) {
        self.realmManager = realmManager
    }
    
    func getMonth(completion: @escaping() -> ()) {
        realmManager.obtain(plainObjectType: Month.self, success: { [weak self] months in
            guard let self = self else { return }
            
            if let existingMonth = months.first(where: { $0.date.monthIsEqualToMonth(date: self.chosenDate) }) {
                self.month = existingMonth
                self.plannedExpenses = existingMonth.plannedExpenses
                completion()
            } else {
                self.month = Month(unixId: Date().timeIntervalSince1970, date: self.chosenDate, days: [])
                self.plannedExpenses = []
                completion()
            }
        })
    }
    
    
    func categoryHasChanged(for expenseWithId: Double, newCategory: ExpenseCategory) {
        if let index = newExpenses.firstIndex(where: { $0.unixId == expenseWithId}) {
            newExpenses[index].expenseCategory = newCategory
        }
    }
    
    func expenseAmountHasChanged(for expenseWithId: Double, newAmount: Int) {
        if let index = newExpenses.firstIndex(where: { $0.unixId == expenseWithId}) {
            newExpenses[index].amount = newAmount
        }
    }
    
    func commentHasChanged(for expenseWithId: Double, newComment: String) {
        if let index = newExpenses.firstIndex(where: { $0.unixId == expenseWithId}) {
            newExpenses[index].comment = newComment
        }
    }
    
    func addNewExpese() {
        let expense = Expense(unixId: Date().timeIntervalSince1970, amount: 0, expenseCategory: .misc, comment: "Без комментария")
        newExpenses.insert(expense, at: 0)
    }
    
    func removeExpense(for expenseWithId: Double) {
        newExpenses.removeAll(where: { $0.unixId == expenseWithId })
        plannedExpenses.removeAll(where: { $0.unixId == expenseWithId })
        month?.removeExpenseIfExist(by: expenseWithId)
    }
    
    func plannedExpenseStatusChanged(for expenseWithId: Double, newStatus: ExpensePlanningStatus) {
        if let index = plannedExpenses.firstIndex(where: { $0.unixId == expenseWithId && $0.planningStaus != newStatus }) {
            plannedExpenses[index].planningStaus = newStatus
        }
    }
    
    func writeNewExpensesToRealm(completion: @escaping() -> ()) {
        guard var month = month else { completion(); return }
        
        month.plannedExpenses = plannedExpenses
        let allExpensesToBeAdded = newExpenses + month.getPlannedExpensesNotYetAddedAsDoneExpenses()
        
        let notDonePlannedExpenses = month.plannedExpenses.filter { $0.planningStaus == .planned }
        notDonePlannedExpenses.forEach { month.removeExpenseIfExist(by: $0.unixId) }
 
        if let existingDayIndex = month.days.firstIndex(where: { $0.date.dayIsEqualToDay(date: chosenDate) }) {
            month.days[existingDayIndex].expenses += allExpensesToBeAdded
        } else {
            var newDay = Day(unixId: Date().timeIntervalSince1970, date: chosenDate, expenses: [])
            newDay.expenses += allExpensesToBeAdded
            month.days.append(newDay)
        }
  
        month.prepareToPutToRealm()
        realmManager.write(plainObject: month, completion: completion)
    }
}
