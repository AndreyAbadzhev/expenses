//
//  AddExpenseConfigurator.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class AddExpenseConfigurator {
    
    func configure(with viewController: AddExpenseViewController, realmManager: RealmManager, completion: @escaping() -> ()) {
        let presenter = AddExpensePresenter()
        let interactor = AddExpenseInteractor(realmManager: realmManager)
        let router = AddExpenseRouter()
        
        viewController.output = presenter
        
        presenter.viewController = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        router.viewController = viewController
        router.completion = completion
    }
}
