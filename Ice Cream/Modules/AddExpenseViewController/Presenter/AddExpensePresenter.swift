//
//  AddExpensePresenter.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class AddExpensePresenter: AddExpenseViewOutput {
    
    weak var viewController: AddExpenseViewInput?
    var interactor: AddExpenseInteractorInput?
    var router: AddExpenseRouterInput?

    var sections: [AddExpensesSection] = []
    
    func viewIsReady() {
        interactor?.getMonth(completion: { [weak self] in
            self?.updateSections()
            self?.viewController?.updateViewState()
        })
    }
    
    func updateSections() {
        sections.removeAll()
        
        if let newExpenses = interactor?.newExpenses {
            sections.append(AddExpensesSection(header: "Новые", items: newExpenses))
        }
        if let plannedExpenses = interactor?.plannedExpenses {
            sections.append(AddExpensesSection(header: "Запланированные", items: plannedExpenses))
        }
    }
    
    func chosenDateHasBeenChanged(to newDate: Date) {
        interactor?.chosenDate = newDate
        interactor?.getMonth(completion: { [weak self] in
            self?.updateSections()
            self?.viewController?.updateViewState()
        })
    }
    
    func categoryButtonDidTap(for expenseWithId: Double) {
        router?.showCategoriesListView(categorySelectedCompletion: { [weak self] selectedCategory in
            self?.interactor?.categoryHasChanged(for: expenseWithId, newCategory: selectedCategory)
            self?.updateSections()
            if let index = self?.sections.getElement(for: 0)?.items.firstIndex(where: { $0.unixId == expenseWithId }) {
                self?.viewController?.updateCell(at: IndexPath(row: index, section: 0))
            }
        })
    }
    
    func expenseAmountHasChanged(for expenseWithId: Double, newAmount: Int) {
        interactor?.expenseAmountHasChanged(for: expenseWithId, newAmount: newAmount)
    }
    
    func commentHasChanged(for expenseWithId: Double, newComment: String) {
        interactor?.commentHasChanged(for: expenseWithId, newComment: newComment)
    }
    
    func addNewRowForExpenseButtonDidTap() {
        interactor?.addNewExpese()
        updateSections()
        viewController?.insertRowToTableView()
    }
    
    func deleteExpenseDidTapped(for indexPath: IndexPath) {
        guard let expense = sections.getElement(for: indexPath.section)?.items.getElement(for: indexPath.row) else { return }
        interactor?.removeExpense(for: expense.unixId)
        updateSections()
        viewController?.removeCell(at: [indexPath])
    }
    
    func plannedExpenseCellDidTap(for indexPath: IndexPath) {
        guard let expense = sections.getElement(for: indexPath.section)?.items.getElement(for: indexPath.row) else { return }
        router?.showPlannedExpenseAlert(for: expense, expenseDoneOrNotCompletion: { [weak self] newStatus in
            self?.interactor?.plannedExpenseStatusChanged(for: expense.unixId, newStatus: newStatus)
            self?.updateSections()
            self?.viewController?.updateCell(at: indexPath)
        })
    }
    
    func doneBarButtonDidTap() {
        interactor?.writeNewExpensesToRealm(completion: { [weak self] in
            self?.router?.expensesCreatedCompletion()
        })
    }
}

struct AddExpensesSection {
    let header: String
    let items: [Expense]
}
