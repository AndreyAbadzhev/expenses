//
//  AddExpenseRouterInput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol AddExpenseRouterInput {

    func showCategoriesListView(categorySelectedCompletion: @escaping (ExpenseCategory) -> ())
    func showPlannedExpenseAlert(for expense: Expense, expenseDoneOrNotCompletion: @escaping(ExpensePlanningStatus) -> ())
    func expensesCreatedCompletion()
}
