//
//  AddExpenseRouter.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit
class AddExpenseRouter: AddExpenseRouterInput {
    
    weak var viewController: AddExpenseViewController?
    var completion: (() -> ())?
    
    func expensesCreatedCompletion() {
        completion?()
    }
    
    func showCategoriesListView(categorySelectedCompletion: @escaping (ExpenseCategory) -> ()) {

        guard let categoriesListView = UIView.loadFromNib(ListSelectCustomView.self) else { return }
        categoriesListView.categorySelectedCompletion = categorySelectedCompletion
        categoriesListView.frame = CGRect(x: 0,
                                           y: 0,
                                           width: UIScreen.main.bounds.width,
                                           height:  UIScreen.main.bounds.height)

        viewController?.view.addSubview(categoriesListView)
    }
    
    func showPlannedExpenseAlert(for expense: Expense, expenseDoneOrNotCompletion: @escaping(ExpensePlanningStatus) -> ()) {
        let alert = UIAlertController(title: "Вы совершили покупку?", message: "В категории \(expense.expenseCategory.rawValue),\nна сумму \(expense.amount),\nс комментарием: \(expense.comment)", preferredStyle: .actionSheet)
        
        let yesAction = UIAlertAction(title: "Да", style: .default, handler: { _ in
            expenseDoneOrNotCompletion(.done)
        })
        
        let noAction = UIAlertAction(title: "Нет", style: .destructive, handler: { _ in
            expenseDoneOrNotCompletion(.planned)
        })
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        
        viewController?.present(alert, animated: true, completion: nil)
    }
    
}
