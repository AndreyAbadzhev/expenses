//
//  AddExpenseViewOutput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol AddExpenseViewOutput {

    var sections: [AddExpensesSection] { get }
    
    func viewIsReady()
    
    func chosenDateHasBeenChanged(to newDate: Date)
    
    func categoryButtonDidTap(for expenseWithId: Double)
    
    func expenseAmountHasChanged(for expenseWithId: Double, newAmount: Int)
    func commentHasChanged(for expenseWithId: Double, newComment: String)
    func addNewRowForExpenseButtonDidTap()
    
    func plannedExpenseCellDidTap(for indexPath: IndexPath)
    
    func deleteExpenseDidTapped(for indexPath: IndexPath)
    
    func doneBarButtonDidTap()
}
