//
//  AddExpenseViewInput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol AddExpenseViewInput: class {
    func updateViewState()
    func insertRowToTableView()
    func updateCell(at indexPath: IndexPath)
    func removeCell(at indexPath: [IndexPath])
}
