//
//  AddExpenseViewController.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

class AddExpenseViewController: UIViewController, AddExpenseViewInput {

    @IBOutlet weak var chooseDateView: UIView!
    @IBOutlet weak var chooseDateTextField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    var output: AddExpenseViewOutput?
    
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
        output?.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    func setupInitialState() {
        title = "Добавить"
        view.addGradientBackground()
        setupNavigationBar()
        setupChooseDateTextField()
        setupTableView()
    }
    
    func setupNavigationBar() {
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneBarButtonDidTap))
        navigationItem.rightBarButtonItem = doneBarButton
    }
    
    func setupChooseDateTextField() {
        chooseDateTextField.text = Date().dayAndWeekdayString
        chooseDateTextField.configureForDatePicking(datePicker: datePicker,
                                                    target: self,
                                                    doneSelector: #selector(chooseDateTextFieldDone),
                                                    cancelSelector: #selector(chooseDateTextFieldCancel))
        chooseDateTextField.textColor = .orangeLight
        chooseDateView.backgroundColor = .blackLight
    }
    
    func setupTableView() {
        tableView.sectionHeaderHeight = 32
        tableView.register(ExpenseCell.self)
    }
    
    @objc func doneBarButtonDidTap() {
        output?.doneBarButtonDidTap()
    }
    
    func updateViewState() {
        self.tableView.reloadData()
    }
    
    func updateCell(at indexPath: IndexPath) {
        self.tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func insertRowToTableView() {
        self.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        guard let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ExpenseCell else { return }
        cell.creationFlowStart()
        
    }
    
    func removeCell(at indexPath: [IndexPath]) {
        self.tableView.deleteRows(at: indexPath, with: .automatic)
    }
    
    @objc func chooseDateTextFieldDone() {
        chooseDateTextField.resignFirstResponder()
        
        let newDate = datePicker.date
        chooseDateTextField.text = newDate.dayAndWeekdayString
        output?.chosenDateHasBeenChanged(to: datePicker.date)
    }
    
    @objc func chooseDateTextFieldCancel() {
        view.endEditing(true)
    }
}

extension AddExpenseViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output?.sections.getElement(for: section)?.items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let expense = output?.sections.getElement(for: indexPath.section)?.items.getElement(for: indexPath.row) else { return UITableViewCell() }
        guard let cell = tableView.deque(ExpenseCell.self) else { return UITableViewCell() }
        cell.fillCell(withDelegate: self, andWithExpense: expense, using: .createMode)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
            self.tableView.beginUpdates(); self.tableView.endUpdates()
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return output?.sections.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.loadFromNib(SectionHeaderView.self)
        view?.fillCell(with: output?.sections[section].header ?? "", and: "0 ₽")
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
            let sectionFooterView = UIView.loadFromNib(AddNewRowForExpenseFooter.self)
            sectionFooterView?.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 66)
            sectionFooterView?.fillCell(delegate: self, withMode: .done)
            return sectionFooterView
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 56 : 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 { output?.plannedExpenseCellDidTap(for: indexPath) }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete { output?.deleteExpenseDidTapped(for: indexPath) }
    }
    

    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
       
        let deleteButton = UIContextualAction(style: .destructive, title: "Удалить\nпокупку", handler: {_,_,_ in
            self.tableView.dataSource?.tableView!(self.tableView, commit: .delete, forRowAt: indexPath)
            return
        })
        deleteButton.backgroundColor = .gradientLight

        let swipeActionsConfiguration = UISwipeActionsConfiguration(actions: [deleteButton])

        return swipeActionsConfiguration
    }
}

extension AddExpenseViewController: AddExpeseCellDelegate {

    func categoryButtonDidTap(for expenseWithId: Double) {
        output?.categoryButtonDidTap(for: expenseWithId)
    }
    
    func expenseAmountHasChanged(for expenseWithId: Double, newAmount: Int) {
        output?.expenseAmountHasChanged(for: expenseWithId, newAmount: newAmount)
    }
    
    func addNewRowForExpenseButtonDidTap() {
        output?.addNewRowForExpenseButtonDidTap()
    }
    
    func commentHasChanged(for expenseWithId: Double, newComment: String) {
        output?.commentHasChanged(for: expenseWithId, newComment: newComment)
        tableView.beginUpdates(); tableView.endUpdates()
    }
    
    func commentExpandButtonDidTap() {
        tableView.beginUpdates(); tableView.endUpdates()
    }
}
