//
//  AddNewRowForExpenseFooter.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

class AddNewRowForExpenseFooter: UIView {

    @IBOutlet weak var _backgroundView: UIView!
    @IBOutlet private weak var moreButton: UIButton!
    
    weak var delegate: AddExpeseCellDelegate?
    
    @IBAction private func moreButtonDidTap() {
        delegate?.addNewRowForExpenseButtonDidTap()
    }
    
    func fillCell(delegate: AddExpeseCellDelegate, withMode expensePlanningStatus: ExpensePlanningStatus) {
        self.delegate = delegate
        
        _backgroundView.backgroundColor = .clear
        
        let title = expensePlanningStatus == .done ? "Добавить" : "Запланировать"
        moreButton.setTitle(title, for: .normal)
        moreButton.setTitleColor(.orangeLight, for: .normal)
    }
    
}
