//
//  AddExpenseCellDelegate.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol AddExpeseCellDelegate: class {
    func categoryButtonDidTap(for expenseWithId: Double)
    
    func expenseAmountHasChanged(for expenseWithId: Double, newAmount: Int)
    func commentHasChanged(for expenseWithId: Double, newComment: String)
    func commentExpandButtonDidTap()
    func addNewRowForExpenseButtonDidTap()
}
