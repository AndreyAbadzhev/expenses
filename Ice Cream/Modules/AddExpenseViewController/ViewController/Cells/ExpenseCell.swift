//
//  ExpenseCell.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 01.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

class ExpenseCell: UITableViewCell {
    
    @IBOutlet weak var _backgroundView: UIView!
    
    @IBOutlet weak var priceTextField: CardTextField!
    @IBOutlet weak var rubleSimbolLabel: CardLabel!
    @IBOutlet weak var categoryButton: UIButton!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var commentTextViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentExpandButton: UIButton!
    
    @IBOutlet weak var notYetBoughtLabel: UILabel!
    
    weak var delegate: AddExpeseCellDelegate?

    var expenseUnixId: Double = 0
    var mode: ExpenseCellMode = .createMode
    var expensePlanningStaus: ExpensePlanningStatus = .done
    
    func fillCell(withDelegate delegate: AddExpeseCellDelegate, andWithExpense expense: Expense, using mode: ExpenseCellMode) {
        
        self._backgroundView.backgroundColor = .blackDark
        self._backgroundView.addShadow()
        
        self.priceTextField.makeCard(filledWith: .blackLight)
        self.priceTextField.textColor = .orangeLight
        self.priceTextField.text = expense.amount == 0 ? "" : String(expense.amount)
        self.priceTextField.configureForPricePicking()
        
        self.rubleSimbolLabel.makeCard(filledWith: .blackLight)
        self.rubleSimbolLabel.textColor = .orangeLight
        self.rubleSimbolLabel.text = "₽"
        
        self.categoryButton.makeCard(filledWith: expense.expenseCategory.categoryColorDark)
        self.categoryButton.setTitleColor(.blackDark, for: .normal)
        self.categoryButton.setTitle(expense.expenseCategory.rawValue, for: .normal)
        
        self.commentExpandButton.makeCard(filledWith: .blackLight)
        self.commentExpandButton.setTitleColor(.orangeLight, for: .normal)
        self.commentExpandButton.setTitle("... развернуть", for: .normal)
        
        self.mode = mode
        self.expensePlanningStaus = expense.planningStaus
        self.expenseUnixId = expense.unixId
        self.delegate = delegate

        
        
        self.commentTextView.makeCard(filledWith: .blackLight)
        self.commentTextView.textContainerInset = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        self.commentTextView.text = expense.comment
        self.commentTextView.textColor = .textLight
        self.commentTextView.configureForCommentEditing()
        
        self.notYetBoughtLabel.textColor = .redLight
        self.notYetBoughtLabel.text = "Не куплено"
        self.notYetBoughtLabel.makeCard(filledWith: .blackLight)
        
        configureCellAccordingToMode()
    }
    
    func configureCellAccordingToMode() {
        let heightOfOneLineInFontOfCommentTextView = (commentTextView.font?.lineHeight ?? 10) + 4 //insets
        let supposedNumberOfLinesInCommentTextView = Int((commentTextView.contentSize.height / heightOfOneLineInFontOfCommentTextView).rounded())
        
        switch mode {
        case .createMode:
            commentTextViewHeightConstraint.constant = commentTextView.contentSize.height
            commentExpandButton.isHidden = true
        case .updateMode:
            commentTextViewHeightConstraint.constant = heightOfOneLineInFontOfCommentTextView
            let visibleNumberOfLines = Int(commentTextViewHeightConstraint.constant / heightOfOneLineInFontOfCommentTextView)
            commentExpandButton.isHidden = visibleNumberOfLines == supposedNumberOfLinesInCommentTextView
        }
        
        switch expensePlanningStaus {
        case .planned:
            self._backgroundView.backgroundColor = .clear
            self._backgroundView.alpha = 0.5
            self.notYetBoughtLabel.isHidden = false
        case .done:
            self._backgroundView.backgroundColor = .blackDark
            self._backgroundView.alpha = 1.0
            self.notYetBoughtLabel.isHidden = true
        }
        
//        if !isChangingAvailable {
//            categoryButton.isUserInteractionEnabled = false
//            priceTextField.isUserInteractionEnabled = false
//            commentTextView.isUserInteractionEnabled = false
//        } else {
//            categoryButton.isUserInteractionEnabled = true
//            priceTextField.isUserInteractionEnabled = true
//            commentTextView.isUserInteractionEnabled = true
//        }
    }
    
    @IBAction func categoryButtonDidTap() {
        delegate?.categoryButtonDidTap(for: expenseUnixId)
    }
    
    @IBAction func commentExpandButtonDidTap() {
        commentTextViewHeightConstraint.constant = commentTextView.contentSize.height
        commentExpandButton.isHidden = !commentExpandButton.isHidden
        delegate?.commentExpandButtonDidTap()
    }
    
    func creationFlowStart() {
        //автоматически выходят окошко с выбором категории и сумма
        delegate?.categoryButtonDidTap(for: expenseUnixId)
    }
}

extension ExpenseCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let newString = textField.text?.replacingCharacters(in: Range(range, in : textField.text!)!, with: string)
        delegate?.expenseAmountHasChanged(for: expenseUnixId, newAmount: Int(newString ?? "") ?? 0)
        return true
    }
}

extension ExpenseCell: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        let newString = textView.text?.replacingCharacters(in: Range(range, in : textView.text!)!, with: text)
        self.commentTextViewHeightConstraint.constant = self.commentTextView.contentSize.height
        let newComment = newString?.isEmpty == true ? "Без комментария" : newString
        delegate?.commentHasChanged(for: expenseUnixId, newComment: newComment ?? "Без комментария")
        
        return true
    }
}


enum ExpenseCellMode {
    case createMode
    case updateMode
}
