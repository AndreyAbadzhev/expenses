//
//  DayDetailedRouter.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 01.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
import UIKit
class DayDetailedRouter: DayDetailedRouterInput {
    
    weak var viewController: DayDetailedViewController?
    let realmManager: RealmManager
    
    init(viewController: DayDetailedViewController?, realmManager: RealmManager) {
        self.viewController = viewController
        self.realmManager = realmManager
    }
    
    var completion: (() -> ())?
    
    func openAddExpenseViewController(/*for day: Day?, in month: Month?,*/ completion: @escaping() -> ()) {
        let addExpenseViewController = AddExpenseViewController()
        let addExpenseConfigurator = AddExpenseConfigurator()
        addExpenseConfigurator.configure(with: addExpenseViewController/*, for: day, in: month*/, realmManager: realmManager, completion: { [weak self] in
            guard let viewController = self?.viewController else { return }
            self?.viewController?.navigationController?.popToViewController(viewController, animated: true)

            completion()
            })
        
        viewController?.navigationController?.pushViewController(addExpenseViewController,
                                                                 animated: true)
    }
    
    func showCategoriesListView(categorySelectedCompletion: @escaping (ExpenseCategory) -> ()) {

        guard let categoriesListView = UIView.loadFromNib(ListSelectCustomView.self) else { return }
        categoriesListView.categorySelectedCompletion = categorySelectedCompletion
        categoriesListView.frame = CGRect(x: 0,
                                           y: 0,
                                           width: UIScreen.main.bounds.width,
                                           height:  UIScreen.main.bounds.height)

        viewController?.view.addSubview(categoriesListView)
    }
    
    func dayInMonthHasChangedCompletion() {
        completion?()
    }
    
}
