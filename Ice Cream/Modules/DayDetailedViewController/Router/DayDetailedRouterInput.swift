//
//  DayDetailedRouterInput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 01.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol DayDetailedRouterInput {
    func openAddExpenseViewController(/*for day: Day?, in month: Month?,*/ completion: @escaping() -> ())
    func showCategoriesListView(categorySelectedCompletion: @escaping (ExpenseCategory) -> ())
    func dayInMonthHasChangedCompletion()
}
