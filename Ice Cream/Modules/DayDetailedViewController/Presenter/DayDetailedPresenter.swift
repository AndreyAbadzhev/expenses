//
//  DayDetailedPresenter.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 01.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class DayDetailedPresenter: DayDetailedViewOutput {
    
    weak var viewController: DayDetailedViewInput?
    var interactor: DayDetailedInteractorInput?
    var router: DayDetailedRouterInput?
    
    var navigationItemTitle: String {
        return interactor?.dayString ?? "Сегодня"
    }
    
    var totalDayExpensesAmounts: Int {
        return interactor?.expenses.reduce(0, { $0 + $1.amount }) ?? 0
    }

    var sections: [DayDetailedSection] = []
    
    func viewIsReady() {
        interactor?.getExpenses()
        updateSections()
        viewController?.updateViewState()
    }
    
    func updateSections() {
        guard let expenses = interactor?.expenses else { return }
        sections.removeAll()
        
        for category in fullCategoriesArray {
            let header = category.rawValue
            let items = expenses.filter { $0.expenseCategory == category }
            
            if !items.isEmpty {
                sections.append(DayDetailedSection(header: header, expenses: items))
            }
        }
    }
    
    func doneBarButtonDidTap() {
        interactor?.writeChangesInMonthToRealm(completion: { [weak self] in
            self?.router?.dayInMonthHasChangedCompletion()
        })
    }
    
    func categoryButtonDidTap(for expenseWithId: Double) {
        router?.showCategoriesListView(categorySelectedCompletion: { [weak self] selectedCategory in
            self?.interactor?.categoryHasChanged(for: expenseWithId, newCategory: selectedCategory)
            self?.updateSections()
            self?.viewController?.updateViewState()
        })
    }
    
    func expenseAmountHasChanged(for expenseWithId: Double, newAmount: Int) {
        interactor?.expenseAmountHasChanged(for: expenseWithId, newAmount: newAmount)
    }
    
    func commentHasChanged(for expenseWithId: Double, newComment: String) {
        interactor?.commentHasChanged(for: expenseWithId, newComment: newComment)
    }
    
    func deleteExpense(at indexPath: IndexPath) -> Bool {
        let sectionsCountBeforeDeletion = sections.count
        let expense = sections.getElement(for: indexPath.section)?.expenses.getElement(for: indexPath.row)
        interactor?.deleteExpense(for: expense?.unixId ?? 0)
        updateSections()
        return sections.count == sectionsCountBeforeDeletion ? true : false // Если удаляемый расход в секции последний, то в контроллер отправляется false и там удаляется целиком секция. В противном случае удаляется только ряд.
    }
}

struct DayDetailedSection {
    
    var header: String
    var expenses: [Expense]

    init(header: String, expenses: [Expense]) {
        self.header = header
        self.expenses = expenses
    }
    
}
