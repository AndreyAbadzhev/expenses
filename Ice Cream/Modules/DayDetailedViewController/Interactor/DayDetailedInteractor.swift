//
//  DayDetailedInteractor.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 01.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class DayDetailedInteractor: DayDetailedInteractorInput {
    
    let realmManager: RealmManager
    var month: Month
    var dayUnixId: Double
    
    var dayString: String {
        if let day = month.days.first(where: { $0.unixId == dayUnixId }) {
            return day.dateString
        } else {
            return "Сегодня"
        }
    }
    
    var expenses: [Expense] = []
    var plannedExpenses: [Expense] = []
    
    init(in month: Month, for dayUnixId: Double, realmManager: RealmManager) {
        self.month = month
        self.dayUnixId = dayUnixId
        self.realmManager = realmManager
    }
    
    func getExpenses() {
        if let day = month.days.first(where: { $0.unixId == dayUnixId }) {
            self.expenses = day.expenses
            self.plannedExpenses = month.plannedExpenses
        }
    }
    
    func categoryHasChanged(for expenseWithId: Double, newCategory: ExpenseCategory) {
        if let index = expenses.firstIndex(where: { $0.unixId == expenseWithId}) {
            expenses[index].expenseCategory = newCategory
        }
    }
    
    func expenseAmountHasChanged(for expenseWithId: Double, newAmount: Int) {
        if let index = expenses.firstIndex(where: { $0.unixId == expenseWithId}) {
            expenses[index].amount = newAmount
        }
    }
    
    func commentHasChanged(for expenseWithId: Double, newComment: String) {
        if let index = expenses.firstIndex(where: { $0.unixId == expenseWithId}) {
            expenses[index].comment = newComment
        }
    }
    
    func deleteExpense(for expenseWithId: Double) {
        expenses.removeAll(where: { $0.unixId == expenseWithId })
        if let index = plannedExpenses.firstIndex(where: { $0.unixId == expenseWithId }) {
            plannedExpenses[index].planningStaus = .planned
        }
    }
    
    func writeChangesInMonthToRealm(completion: @escaping() -> ()) {
        if let index = month.days.firstIndex(where: { $0.unixId == dayUnixId }) {
            month.days[index].expenses = expenses
            month.days[index].prepareToPutToRealm()
            
            month.plannedExpenses = plannedExpenses
 
            realmManager.write(plainObject: month, completion: completion)
        }
    }
}
