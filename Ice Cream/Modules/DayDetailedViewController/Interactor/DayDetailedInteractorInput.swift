//
//  DayDetailedInteractorInput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 01.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol DayDetailedInteractorInput {
    var dayString: String { get }
    
    var expenses: [Expense] { get }
    
    func getExpenses()
    
    func categoryHasChanged(for expenseWithId: Double, newCategory: ExpenseCategory)
    func expenseAmountHasChanged(for expenseWithId: Double, newAmount: Int)
    func commentHasChanged(for expenseWithId: Double, newComment: String)
    
    func deleteExpense(for expenseWithId: Double)
    
    func writeChangesInMonthToRealm(completion: @escaping() -> ())
}
