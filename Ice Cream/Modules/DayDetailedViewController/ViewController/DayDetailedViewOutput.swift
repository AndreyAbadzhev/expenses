//
//  DayDetailedViewOutput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 01.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol DayDetailedViewOutput {
    var navigationItemTitle: String { get }
    var totalDayExpensesAmounts: Int { get }
    var sections: [DayDetailedSection] { get }
    
    func viewIsReady()
    
    func categoryButtonDidTap(for expenseWithId: Double)
    
    func expenseAmountHasChanged(for expenseWithId: Double, newAmount: Int)
    func commentHasChanged(for expenseWithId: Double, newComment: String)
    
    func deleteExpense(at indexPath: IndexPath) -> Bool
    
    func doneBarButtonDidTap()
}
