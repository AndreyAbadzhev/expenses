//
//  DayDetailedViewController.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 01.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

class DayDetailedViewController: UIViewController, DayDetailedViewInput {

    @IBOutlet weak var addExpenseButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    var output: DayDetailedViewOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
        output?.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    func setupInitialState() {
        view.addGradientBackground()
        setupNavigationBar()
        setupTableView()
    }
    
    func setupNavigationBar() {
        title = output?.navigationItemTitle
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneBarButtonDidTap))
        navigationItem.rightBarButtonItem = doneBarButton
    }
    
    func setupTableView() {
        tableView.register(ExpenseCell.self)
        tableView.sectionHeaderHeight = 32
        tableView.sectionFooterHeight = 24
    }

    func updateViewState() {
        self.tableView.reloadData()
    }
    
    @objc func doneBarButtonDidTap() {
        output?.doneBarButtonDidTap()
    }
    
    @IBAction func addExpenseButtonDidTap() {
       // output?.addExpenseButtonDidTap()
    }
    
}

extension DayDetailedViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return output?.sections.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output?.sections.getElement(for: section)?.expenses.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let expense = output?.sections.getElement(for: indexPath.section)?.expenses.getElement(for: indexPath.row) else { return UITableViewCell() }
        guard let cell = tableView.deque(ExpenseCell.self) else { return UITableViewCell() }
        cell.fillCell(withDelegate: self, andWithExpense: expense, using: .updateMode)
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if output?.deleteExpense(at: indexPath) == true {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            } else {
                let indexSet = IndexSet(arrayLiteral: indexPath.section)
                tableView.deleteSections(indexSet, with: .automatic)
            }
        }
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
       
        let deleteButton = UIContextualAction(style: .destructive, title: "Удалить\nпокупку", handler: {_,_,_ in
            self.tableView.dataSource?.tableView!(self.tableView, commit: .delete, forRowAt: indexPath)
            return
        })
        deleteButton.backgroundColor = .gradientLight

        let swipeActionsConfiguration = UISwipeActionsConfiguration(actions: [deleteButton])

        return swipeActionsConfiguration
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.loadFromNib(SectionHeaderForExpenseCategoryView.self)
        
        
        let expenseCategory = output?.sections.getElement(for: section)?.expenses.getElement(for: 0)?.expenseCategory ?? .misc
        let categoryTotalAmount = output?.sections.getElement(for: section)?.expenses.reduce(0, { $0 + $1.amount }) ?? 0
        
        view?.fillCell(with: expenseCategory, totalAmount: categoryTotalAmount)
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
//            self.tableView.beginUpdates()
//            self.tableView.endUpdates()
//        })
    }
}

extension DayDetailedViewController: AddExpeseCellDelegate {
    func categoryButtonDidTap(for expenseWithId: Double) {
        output?.categoryButtonDidTap(for: expenseWithId)
    }
    
    func expenseAmountHasChanged(for expenseWithId: Double, newAmount: Int) {
        output?.expenseAmountHasChanged(for: expenseWithId, newAmount: newAmount)
    }
    
    func commentHasChanged(for expenseWithId: Double, newComment: String) {
        output?.commentHasChanged(for: expenseWithId, newComment: newComment)
        tableView.beginUpdates(); tableView.endUpdates()
    }
    
    func commentExpandButtonDidTap() {
        tableView.beginUpdates(); tableView.endUpdates()
    }
    
    func addNewRowForExpenseButtonDidTap() {
        // Не используется
    }
}
