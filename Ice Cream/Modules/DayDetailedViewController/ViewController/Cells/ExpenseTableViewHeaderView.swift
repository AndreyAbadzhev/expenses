//
//  ExpenseTableViewHeaderView.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

class ExpenseTableViewHeaderView: UIView {

    @IBOutlet weak var _backgroundView: UIView!
    
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
    
    func fillCell(with color: UIColor, leftText: String, rightText: String) {
        _backgroundView.backgroundColor = color
        
        
        leftLabel.textColor = .purpleDark
        rightLabel.textColor = .purpleDark
        
        leftLabel.text = leftText
        rightLabel.text = rightText
    }


}
