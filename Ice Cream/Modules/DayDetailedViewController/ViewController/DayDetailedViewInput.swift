//
//  DayDetailedViewInput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 01.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol DayDetailedViewInput: class {
    func updateViewState()
}
