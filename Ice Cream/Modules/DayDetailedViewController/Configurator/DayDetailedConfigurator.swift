//
//  DayDetailedConfigurator.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 01.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class DayDetailedConfigurator {
    
    func configure(with viewController: DayDetailedViewController, in month: Month, for dayUnixId: Double, realmManager: RealmManager, completion: @escaping() -> ()) {
        
        let presenter = DayDetailedPresenter()
        let router = DayDetailedRouter(viewController: viewController, realmManager: realmManager)
        let interactor = DayDetailedInteractor(in: month, for: dayUnixId, realmManager: realmManager)
        
        viewController.output = presenter
        
        presenter.viewController = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        router.viewController = viewController
        router.completion = completion
        
    }
    
}
