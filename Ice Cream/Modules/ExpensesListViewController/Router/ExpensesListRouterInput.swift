//
//  ShopRouterInput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 26.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol ExpensesListRouterInput {
    func openAddExpenseViewController(completion: @escaping() -> ())
    func openDayDetailedViewController(in month: Month, for dayUnixId: Double, completion: @escaping() -> ())
}
