//
//  ShopRouter.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 26.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class ExpensesListRouter: ExpensesListRouterInput {
    
    weak var viewController: ExpensesListViewController?
    let realmManager: RealmManager
    
    init(with viewController: ExpensesListViewController, realmManager: RealmManager) {
        self.viewController = viewController
        self.realmManager = realmManager
    }
    
    func openAddExpenseViewController( completion: @escaping() -> ()) {
        let addExpenseViewController = AddExpenseViewController()
        let addExpenseConfigurator = AddExpenseConfigurator()
        addExpenseConfigurator.configure(with: addExpenseViewController, realmManager: realmManager, completion: { [weak self] in
            guard let viewController = self?.viewController else { return }
            self?.viewController?.navigationController?.popToViewController(viewController, animated: true)
            completion()
            })
        
        viewController?.navigationController?.pushViewController(addExpenseViewController, animated: true)
    }
    
    func openDayDetailedViewController(in month: Month, for dayUnixId: Double, completion: @escaping() -> ()) {
        let dayDetailedViewController = DayDetailedViewController()
        let dayDetailedConfigurator = DayDetailedConfigurator()
        dayDetailedConfigurator.configure(with: dayDetailedViewController, in: month, for: dayUnixId, realmManager: realmManager, completion: { [weak self] in
            
            guard let viewController = self?.viewController else { return }
            self?.viewController?.navigationController?.popToViewController(viewController, animated: true)
            completion()
        })
        
        viewController?.navigationController?.pushViewController(dayDetailedViewController, animated: true)
    }
}
