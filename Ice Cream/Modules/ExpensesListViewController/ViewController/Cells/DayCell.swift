//
//  DayCell.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

class DayCell: UITableViewCell {
    
    @IBOutlet weak var _backgroundView: UIView!
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet private weak var dayLabel: UILabel!
    @IBOutlet weak var totalExpensesLabel: UILabel!
    @IBOutlet private weak var expensesTableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    var day: Day?
    
    func fillCell(with day: Day) {
        self.day = day
        self.dayLabel.text = day.dateString.lowercased()
        self.totalExpensesLabel.text = "\(day.totalExpenses) ₽"

        _backgroundView.addShadow()
        
//        headerView.backgroundColor = .blackLight
        _backgroundView.backgroundColor = .blackDark
        
        dayLabel.textColor = .textLight
        totalExpensesLabel.textColor = .orangeLight
        
        var headerViewWidth: CGFloat = 16
        headerViewWidth += dayLabel.intrinsicContentSize.width
        headerViewWidth += 8
        headerViewWidth += totalExpensesLabel.intrinsicContentSize.width
        
        headerView.addGradientHorizontal(mainColor: .blackLight, additionalColor: .blackDark, visibleWidth: headerViewWidth)
        
        configureTableView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        expensesTableView.reloadData()
        tableViewHeightConstraint.constant = expensesTableView.contentSize.height
    }
    
    func configureTableView() {
        expensesTableView.isUserInteractionEnabled = false
        expensesTableView.register(CombinedExpencesCell.self)
        expensesTableView.rowHeight = 28
        expensesTableView.estimatedRowHeight = 28
    }
    
}

extension DayCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return day?.expensesCombined.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let expence = day?.expensesCombined.getElement(for: indexPath.row) else { return UITableViewCell() }
        guard let cell = tableView.deque(CombinedExpencesCell.self) else { return UITableViewCell() }
        cell.fillCell(with: expence)
        return cell
    }
}
