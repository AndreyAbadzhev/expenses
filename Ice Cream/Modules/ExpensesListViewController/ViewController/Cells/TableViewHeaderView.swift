//
//  TableViewHeaderView.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 28.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

class TableViewHeaderView: UIView {
    
    @IBOutlet weak var _backgroundView: UIView!
    
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var totalExpensesAmountsLabel: UILabel!
    
    func fillCell(with color: UIColor) {
        _backgroundView.backgroundColor = color
        
        
        monthLabel.textColor = .textLight
        totalExpensesAmountsLabel.textColor = .orangeLight
        
    }

}
