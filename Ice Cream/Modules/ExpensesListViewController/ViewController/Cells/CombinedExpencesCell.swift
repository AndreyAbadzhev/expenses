//
//  ExpenceCell.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

class CombinedExpencesCell: UITableViewCell {
    
    @IBOutlet private weak var colorDotView: UIView!
    @IBOutlet private weak var categoryNameLabel: CardLabel!
    @IBOutlet private weak var dotsLabel: UILabel!
    @IBOutlet private weak var priceLabel: CardLabel!
    
    func fillCell(with expense: Expense) {
        colorDotView.clipsToBounds = true
        colorDotView.layer.cornerRadius = 6
        colorDotView.backgroundColor = expense.expenseCategory.categoryColorDark

        
        self.categoryNameLabel.text = expense.expenseCategory.rawValue
        self.priceLabel.text = "\(expense.amount) ₽"
        
//        categoryNameLabel.makeCard(filledWith: expense.expenseCategory.categoryColorDark)
        priceLabel.makeCard(filledWith: UIColor.blackLight)
        categoryNameLabel.textColor = .textLight
        dotsLabel.textColor = .textDark
        priceLabel.textColor = .orangeLight
    }
    
}
