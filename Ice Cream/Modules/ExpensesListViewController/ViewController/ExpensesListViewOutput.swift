//
//  ShopViewOutput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 26.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol ExpensesListViewOutput {
    var sections: [ExpensesListSection] { get }
    
    func viewIsReady()

    func addExpenseButtonDidTap()
    func dayCellDidTap(at indexPath: IndexPath)
}
