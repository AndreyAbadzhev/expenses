//
//  ShopViewController.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 26.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

class ExpensesListViewController: UIViewController, ExpensesListViewInput {

    @IBOutlet weak var maskView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var output: ExpensesListViewOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
        output?.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    func setupInitialState() {
        view.addGradientBackground()
        maskView.addMask()
        setupNavigationBar()
        setupTableView()
    }
    
    func updateState() {
        self.tableView.reloadData()
    }
    
    func setupTableView() {
        tableView.register(DayCell.self)
        tableView.sectionHeaderHeight = 32
    }
    
    func setupNavigationBar() {
        title = "Расходы"
        let addNewExpenseBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addExpenseButtonDidTap))
        navigationItem.rightBarButtonItem = addNewExpenseBarButton
    }
    
    @objc func addExpenseButtonDidTap() {
        output?.addExpenseButtonDidTap()
    }

}

extension ExpensesListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return output?.sections.count ?? 0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.loadFromNib(TableViewHeaderView.self)
        view?.monthLabel.text = output?.sections[section].header
        view?.totalExpensesAmountsLabel.text = "\(output?.sections[section].month.totalExpensesAmounts ?? 0) ₽"
        view?.fillCell(with: .blackLight)
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output?.sections[section].days.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = output?.sections.getElement(for: indexPath.section) else { return UITableViewCell() }
        guard let day = section.days.getElement(for: indexPath.row) else { return UITableViewCell() }
        guard let cell = tableView.deque(DayCell.self) else { return UITableViewCell() }
        cell.fillCell(with: day)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output?.dayCellDidTap(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        })
    }

}
