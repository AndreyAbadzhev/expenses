//
//  ShopInteractor.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 26.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class ExpensesListInteractor: ExpensesListInteractorInput {
    
    let realmManager: RealmManager
    
    var months: [Month] = []
    
    init(realmManager: RealmManager) {
        self.realmManager = realmManager
    }
    
    func loadMonthsFormRealm(success: @escaping() -> ()) {
        realmManager.obtain(plainObjectType: Month.self, success: { [weak self] months in
            self?.months = months
            success()
        })
    }
}
