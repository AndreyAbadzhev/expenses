//
//  ShopInteractorInput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 26.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol ExpensesListInteractorInput {
    var months: [Month] { get }
    
    func loadMonthsFormRealm(success: @escaping() -> ())
}
