//
//  ShopConfigurator.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 26.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class ExpensesListConfigurator {

    func configure(with viewController: ExpensesListViewController, realmManager: RealmManager) {
        let presenter = ExpensesListPresenter()
        let interactor = ExpensesListInteractor(realmManager: realmManager)
        let router = ExpensesListRouter(with: viewController, realmManager: realmManager)
        
        viewController.output = presenter
        
        presenter.viewController = viewController
        presenter.interactor = interactor
        presenter.router = router
    }
    
}
