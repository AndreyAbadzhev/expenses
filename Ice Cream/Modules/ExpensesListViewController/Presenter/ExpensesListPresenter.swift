//
//  ShowPresenter.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 26.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class ExpensesListPresenter: ExpensesListViewOutput {
    
    weak var viewController: ExpensesListViewInput?
    var interactor: ExpensesListInteractorInput?
    var router: ExpensesListRouterInput?
    
    var sections: [ExpensesListSection] = []
    
    func viewIsReady() {
        interactor?.loadMonthsFormRealm { [weak self] in
            self?.updateSections()
            self?.viewController?.updateState()
        }
    }
    
    func updateSections() {
        guard let months = interactor?.months else { return }
        let sortedMonths = months.filter { !$0.days.isEmpty }.sorted { $0.date > $1.date }
        self.sections = sortedMonths.map { ExpensesListSection(month: $0) }
    }
    
    func addExpenseButtonDidTap() {
        router?.openAddExpenseViewController(completion: { [weak self] in
            self?.interactor?.loadMonthsFormRealm {
                self?.updateSections()
                self?.viewController?.updateState()
            }
        })
    }
    
    func dayCellDidTap(at indexPath: IndexPath) {
        guard let chosenMonth = sections.getElement(for: indexPath.section)?.month else { return }
        guard let chosenDay = chosenMonth.days.getElement(for: indexPath.row) else { return }
        router?.openDayDetailedViewController(in: chosenMonth, for: chosenDay.unixId, completion: { [weak self] in
            self?.interactor?.loadMonthsFormRealm {
                self?.updateSections()
                self?.viewController?.updateState()
            }
        })
    }
}

struct ExpensesListSection {
    let month: Month
    
    var header: String { return month.dateString }
    var days: [Day] { return month.days }
    
    init(month: Month) {
        self.month = month
    }
}
