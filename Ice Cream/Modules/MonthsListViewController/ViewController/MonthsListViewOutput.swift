//
//  MonthsListViewOutput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 22.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol MonthsListViewOutput {
    
    var sections: [MonthListSection] { get }
    
    func viewIsReady()
    func viewWillAppear()
    
    func monthCellDidTapped(at indexPath: IndexPath)
    
}
