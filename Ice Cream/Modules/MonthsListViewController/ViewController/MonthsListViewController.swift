//
//  MonthsListViewController.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 22.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

class MonthsListViewController: UIViewController, MonthsListViewInput {

    @IBOutlet weak var maskView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var output: MonthsListViewOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
        output?.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
        output?.viewWillAppear()
    }
    
    func setupInitialState() {
        view.addGradientBackground()
        maskView.addMask()
        setupNavigationBar()
        setupTableView()
    }
    
    func updateState() {
        tableView.reloadData()
    }
    
    func setupNavigationBar() {
        title = "Бюджет"
    }
    
    func setupTableView() {
        tableView.register(MonthCell.self)
        tableView.rowHeight = 44
        tableView.sectionHeaderHeight = 32
    }

}

extension MonthsListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output?.sections.getElement(for: section)?.months.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let month = output?.sections.getElement(for: indexPath.section)?.months.getElement(for: indexPath.row) else { return UITableViewCell() }
        guard let cell = tableView.deque(MonthCell.self) else { return UITableViewCell() }
        cell.fillCell(with: month)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return output?.sections.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.loadFromNib(TableViewHeaderView.self)
        view?.monthLabel.text = output?.sections.getElement(for: section)?.header ?? "Текущий год"
        view?.totalExpensesAmountsLabel.text = ""
        view?.fillCell(with: .blackLight)
//        view?.addShadow()
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output?.monthCellDidTapped(at: indexPath)
    }
}
