//
//  MonthCell.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 22.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

class MonthCell: UITableViewCell {

    @IBOutlet weak var _backgroundView: UIView!
    
    @IBOutlet weak var monthNameLabel: UILabel!
    @IBOutlet weak var expensesPlanLabel: CardLabel!
    
    func fillCell(with month: Month) {
        
        _backgroundView.makeCard(filledWith: .redDark)
        
        monthNameLabel.text = month.date.monthAndYearString
        monthNameLabel.textColor = .purpleDark
        
        expensesPlanLabel.textColor = .purpleDark
        
        switch true {
        case month.totalPlannedExpensesAmounts > 0 && month.totalExpensesAmounts == 0:
            expensesPlanLabel.text = "План: \(month.totalPlannedExpensesAmounts)"
            expensesPlanLabel.makeCard(filledWith: .greenLight)
        case month.totalPlannedExpensesAmounts > 0 && month.totalExpensesAmounts > 0:
            expensesPlanLabel.text = "План: \(month.totalPlannedExpensesAmounts) - потрачено: \(month.totalExpensesAmounts)"
            expensesPlanLabel.makeCard(filledWith: .redWarning)
        default:
            expensesPlanLabel.text = "Не запланировано"
            expensesPlanLabel.makeCard(filledWith: .redLight)
        }
    }
    
}
