//
//  MonthsListPresenter.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 22.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class MonthsListPresenter: MonthsListViewOutput {
    
    weak var viewController: MonthsListViewInput?
    var interactor: MonthsListInteractorInput?
    var router: MonthsListRouterInput?
    
    var sections: [MonthListSection] = []
    
    func viewIsReady() {
        
    }
    
    func viewWillAppear() {
        interactor?.fillMonthsArray(completion: { [weak self] in
            self?.updateSections()
            self?.viewController?.updateState()
        })
    }
    
    func updateSections() {
        guard let interactor = interactor else { return }
        sections.removeAll()
        
        let monthsInCurrentYear = interactor.months.filter { $0.date.dateInCurrentYear() }
        let monthsInNextYear = interactor.months.filter { $0.date.dateInNextYear() }
        
        
        if !monthsInCurrentYear.isEmpty {
            sections.append(MonthListSection(header: "Текущий год", months: monthsInCurrentYear))
        }
        
        if !monthsInNextYear.isEmpty {
            sections.append(MonthListSection(header: "Следующий год", months: monthsInNextYear))
        }
    }
    
    func monthCellDidTapped(at indexPath: IndexPath) {
        guard let unixId = sections.getElement(for: indexPath.section)?.months.getElement(for: indexPath.row)?.unixId else { return }
        guard let month = interactor?.getMonth(for: unixId) else { return }
        
        self.router?.openManageMonthPlanViewController(with: month)
    }
}

struct MonthListSection {
    let header: String
    let months: [Month]
}
