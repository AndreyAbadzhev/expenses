//
//  MonthsListConfigurator.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 22.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class MonthsListConfigurator {
    
    func configure(with viewController: MonthsListViewController, realmManager: RealmManager) {
        let presenter = MonthsListPresenter()
        let interactor = MonthsListInteractor(realmManager: realmManager)
        let router = MonthsListRouter(with: viewController, realmManager: realmManager)
        
        viewController.output = presenter
        
        presenter.viewController = viewController
        presenter.interactor = interactor
        presenter.router = router
    }
    
}
