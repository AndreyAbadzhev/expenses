//
//  MonthsListInteractorInput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 22.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol MonthsListInteractorInput {
    
    var months: [Month] { get }
    
    func fillMonthsArray(completion: @escaping() -> ())
    func getMonth(for unixId: Double) -> Month?
}
