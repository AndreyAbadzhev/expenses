//
//  MonthsListInteractor.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 22.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class MonthsListInteractor: MonthsListInteractorInput {
    
    let realmManager: RealmManager
    var months: [Month] = []
    
    init(realmManager: RealmManager) {
        self.realmManager = realmManager
    }
    
    func fillMonthsArray(completion: @escaping() -> ()) {
        months.removeAll()
        realmManager.obtain(plainObjectType: Month.self, success: { [weak self] months in
            for i in 0..<12 {
                if let date = Calendar.current.date(byAdding: .month, value: i, to: Date()) {
                    if let existingMonth = months.first(where: { $0.date.monthIsEqualToMonth(date: date) }) {
                        self?.months.append(existingMonth)
                    } else {
                        self?.months.append(Month(unixId: Date().timeIntervalSince1970, date: date, days: []))
                    }
                }
            }
            completion()
        })
    }
    
    func getMonth(for unixId: Double) -> Month? {
        return months.first(where: { $0.unixId == unixId })
    }
}
