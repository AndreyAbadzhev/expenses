//
//  MonthsListRouter.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 22.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
class MonthsListRouter: MonthsListRouterInput {
    
    weak var viewController: MonthsListViewController?
    var realmManager: RealmManager
    
    init(with viewController: MonthsListViewController, realmManager: RealmManager) {
        self.viewController = viewController
        self.realmManager = realmManager
    }
    
    func openManageMonthPlanViewController(with month: Month) {
        let manageMonthPlanViewController = ManageMonthPlanViewController()
        let manageMonthPlanConfigurator = ManageMonthPlanConfigurator()
        manageMonthPlanConfigurator.configure(with: manageMonthPlanViewController, realmManager: self.realmManager, month: month, completion: { [weak self] in
            guard let self = self, let viewController = self.viewController else { return }
            self.viewController?.navigationController?.popToViewController(viewController, animated: true)
        })
        viewController?.navigationController?.pushViewController(manageMonthPlanViewController, animated: true)
    }
    
}
