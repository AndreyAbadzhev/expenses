//
//  MonthsListRouterInput.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 22.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol MonthsListRouterInput {
    func openManageMonthPlanViewController(with month: Month)
}
