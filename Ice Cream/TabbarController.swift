//
//  TabbarController.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 22.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

class TabbarController: UITabBarController {

    let realmManager: RealmManager = RealmDataBase()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addControllers()
        configureTabbarItems()
    }
    
    func addControllers() {
        let expensesListViewController = ExpensesListViewController()
        let configurator = ExpensesListConfigurator()
        configurator.configure(with: expensesListViewController, realmManager: realmManager)
        let expensesListNavigationController = UINavigationController(rootViewController: expensesListViewController)
        expensesListNavigationController.tabBarItem = UITabBarItem(title: "Расходы", image: UIImage(named: "RubleImage"), tag: 0)
        
        
        let monthsListViewController = MonthsListViewController()
        let monthsListConfigurator = MonthsListConfigurator()
        monthsListConfigurator.configure(with: monthsListViewController, realmManager: realmManager)
        let monthsListNavigationController = UINavigationController(rootViewController: monthsListViewController)
        monthsListNavigationController.tabBarItem = UITabBarItem(title: "Бюджет", image: UIImage(named: "PlanImage"), tag: 1)
        
        self.viewControllers = [expensesListNavigationController, monthsListNavigationController]
    }
    
    func configureTabbarItems() {
        
    }
}
