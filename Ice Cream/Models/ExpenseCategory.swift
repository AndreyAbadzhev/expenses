//
//  ExpenceCategory.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

let fullCategoriesArray: [ExpenseCategory] = [.food, .pets, .misc, .credits, .accumulation, .transport, .utilities]

enum ExpenseCategory: String {
   
    case misc = "Разное"
    
    case food = "Еда"
    case pets = "Животные"
    case transport = "Транспорт"
    
    case credits = "Кредиты"
    case accumulation = "Отложено"
    case utilities = "Коммунальные услуги"
    
    var categoryColorDark: UIColor {
        switch self {
        case .food: return .foodDark
        case .misc: return .miscDark
        case .pets: return .petsDark
        case .utilities: return .utilitiesDark
        case .credits: return .creditsDark
        case .accumulation: return .accumulationDark
        case .transport: return .transportDark
            
        default: return .blackLight
            
        }
    }
    
    var categoryColorLight: UIColor {
        switch self {
        case .food: return .foodLight
        case .misc: return .miscLight
        case .pets: return .petsLight
        case .utilities: return .utilitiesLight
        case .credits: return .creditsLight
        case .accumulation: return .accumulationLight
        case .transport: return .transportLight
            
        default: return .blackLight
            
        }
    }
}

//enum ExpenceSubcategory: Int {
//
//}
