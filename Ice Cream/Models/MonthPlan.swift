//
//  MonthPlan.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 17.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
import RealmSwift

struct PlannedExpensesByCategory {
    
    var expenseCategory: ExpenseCategory
    var plannedAmount: Int = 0
    var factAmount: Int = 0
    
    init(expenseCategory: ExpenseCategory, plannedAmount: Int, factAmount: Int = 0) {
        self.expenseCategory = expenseCategory
        self.plannedAmount = plannedAmount
        self.factAmount = factAmount
    }
    
    init(plannedExpensesByCategoryRealm: PlannedExpensesByCategoryRealm) {
        self.expenseCategory = ExpenseCategory(rawValue: plannedExpensesByCategoryRealm.expenseCategory) ?? .misc
        self.plannedAmount = plannedExpensesByCategoryRealm.plannedAmount
        self.factAmount = plannedExpensesByCategoryRealm.factAmount
    }
}

class PlannedExpensesByCategoryRealm: Object {
 
    @objc dynamic var expenseCategory: String = ""
    @objc dynamic var plannedAmount: Int = 0
    @objc dynamic var factAmount: Int = 0
    
    convenience init(plannedExpensesByCategory: PlannedExpensesByCategory) {
        self.init()
        self.expenseCategory = plannedExpensesByCategory.expenseCategory.rawValue
        self.plannedAmount = plannedExpensesByCategory.plannedAmount
        self.factAmount = plannedExpensesByCategory.factAmount
    }
}
