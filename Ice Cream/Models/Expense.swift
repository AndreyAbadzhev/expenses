//
//  Expence.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
import RealmSwift
struct Expense: Hashable {

    var unixId: Double
    
    var amount: Int
    var expenseCategory: ExpenseCategory
    var comment: String
    
    var planningStaus: ExpensePlanningStatus
    
    
    init(unixId: Double, amount: Int, expenseCategory: ExpenseCategory, comment: String, planningStaus: ExpensePlanningStatus = .done) {
        self.unixId = unixId
        self.amount = amount
        self.expenseCategory = expenseCategory
        self.comment = comment
        self.planningStaus = planningStaus
    }
    
    init(expenseRealm: ExpenseRealm) {
        self.unixId = expenseRealm.unixId
        self.amount = expenseRealm.amount
        self.expenseCategory = ExpenseCategory(rawValue: expenseRealm.expenseCategory) ?? .misc
        self.comment = expenseRealm.comment
        self.planningStaus = ExpensePlanningStatus(rawValue: expenseRealm.planningStaus) ?? .done
    }
    
}

class ExpenseRealm: Object {
    
    @objc dynamic var unixId: Double = 0

    @objc dynamic var amount: Int = 0
    @objc dynamic var expenseCategory: String = ""
    @objc dynamic var comment: String = ""
    
    @objc dynamic var planningStaus: Int = 0
    
    convenience init(expense: Expense) {
        self.init()
        self.unixId = expense.unixId
        self.amount = expense.amount
        self.expenseCategory = expense.expenseCategory.rawValue
        self.comment = expense.comment
        self.planningStaus = expense.planningStaus.rawValue
    }
}


enum ExpensePlanningStatus: Int {
    case done = 0
    case planned = 1
}
