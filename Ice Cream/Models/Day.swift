//
//  Day.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
import RealmSwift
struct Day: RealmConvertible {
    
    var unixId: Double
    
    var date: Date
    var dateString: String { return date.dayAndWeekdayString }
    var expenses: [Expense]
    var expensesCombined: [Expense] {
        var expensesCombined = [Expense]()
        
        for expense in expenses {
            if expensesCombined.contains(where: { $0.expenseCategory == expense.expenseCategory }) { continue }
            
            let filteredExpensesByCategory = expenses.filter { $0.expenseCategory == expense.expenseCategory }
            let totalAmountOfFilteredExpensesByCategoryArray: [Int] = filteredExpensesByCategory.map { $0.amount }
            let totalAmountOfFilteredExpensesByCategory = totalAmountOfFilteredExpensesByCategoryArray.reduce(0, +)
            let category = expense.expenseCategory
            let combinedExpense = Expense(unixId: 0, amount: totalAmountOfFilteredExpensesByCategory, expenseCategory: category, comment: "Без комментария")
            expensesCombined.append(combinedExpense)
        }
        return expensesCombined.sorted { $0.expenseCategory.rawValue < $1.expenseCategory.rawValue }
    }
    
//    var expensesSortedByCategories: [[Expense]] {
//        var expensesSortedByCategories: [[Expense]] = []
//
//        for expense in expenses {
//            if expensesSortedByCategories.contains(where: { $0.contains(where: { $0.expenseCategory == expense.expenseCategory })}) { continue }
//
//            let allExpensesForOneCategory = expenses.filter { $0.expenseCategory == expense.expenseCategory }
//            expensesSortedByCategories.append(allExpensesForOneCategory)
//        }
//
//        return expensesSortedByCategories.sorted { $0.first?.expenseCategory.rawValue ?? "" < $1.first?.expenseCategory.rawValue ?? "" }
//    }
    
    var totalExpenses: Int {
        let amountsArray = expenses.map { $0.amount }
        return amountsArray.reduce(0, +)
    }
    
    init(unixId: Double, date: Date, expenses: [Expense]) {
        self.unixId = unixId
        self.date = date
        self.expenses = expenses
    }
    
    init(dayRealm: DayRealm) {
        self.unixId = Double(dayRealm.unixId) ?? 0
        self.date = dayRealm.date
        self.expenses = dayRealm.expenses.map { Expense(expenseRealm: $0) }
    }
    
    mutating func prepareToPutToRealm() {
        expenses = expenses.filter { $0.amount > 0 }
    }
    
    func convertToRealmModel() -> PlainConvertible {
        return DayRealm(day: self)
    }
    
}

class DayRealm: Object, PlainConvertible {
    
    @objc dynamic var unixId: String = ""
    
    @objc dynamic var date: Date = Date()
    dynamic var expenses = List<ExpenseRealm>()
    
    convenience init(day: Day) {
        self.init()
        self.unixId = String(day.unixId)
        self.date = day.date
        self.expenses.append(objectsIn: day.expenses.map { ExpenseRealm(expense: $0) })
    }
    
    override static func primaryKey() -> String? {
        return "unixId"
    }
    
    func convertToPlainModel() -> RealmConvertible {
        return Day(dayRealm: self)
    }
}
