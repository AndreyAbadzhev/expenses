//
//  Month.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
import RealmSwift
struct Month: RealmConvertible {
    
   // typealias RealmModelType = MonthRealm
    
    var unixId: Double
    
    let date: Date
    var dateString: String { return date.monthAndYearString }
    var days: [Day]
    
    var plannedExpensesByCategory: [PlannedExpensesByCategory]
    var plannedExpenses: [Expense]
    
    var totalPlannedExpensesAmounts: Int {
        var totalPlannedExpenses: Int = 0
        plannedExpensesByCategory.forEach { totalPlannedExpenses += $0.plannedAmount }
        plannedExpenses.forEach { totalPlannedExpenses += $0.amount }
        return totalPlannedExpenses
    }
    
    var totalExpensesAmounts: Int {
        var totalExpensesAmounts: Int = 0
        days.forEach { $0.expenses.forEach { totalExpensesAmounts += $0.amount } }
        return totalExpensesAmounts
    }
    
    var allExpenses: [Expense] {
        return days.reduce([], { $0 + $1.expenses })
    }
    
    init(unixId: Double, date: Date = Date(), days: [Day], plannedExpensesByCategory: [PlannedExpensesByCategory] = [], plannedExpenses: [Expense] = []) {
        self.unixId = unixId
        self.date = date
        self.days = days
        self.plannedExpensesByCategory = plannedExpensesByCategory
        self.plannedExpenses = plannedExpenses
    }
    
    init(monthRealm: MonthRealm) {
        self.unixId = Double(monthRealm.unixId) ?? 0
        self.date = monthRealm.date
        self.days = monthRealm.days.map { Day(dayRealm: $0) }
        self.plannedExpensesByCategory = monthRealm.plannedExpensesByCategory.map { PlannedExpensesByCategory(plannedExpensesByCategoryRealm: $0) }
        self.plannedExpenses = monthRealm.plannedExpenses.map { Expense(expenseRealm: $0) }
    }
    
    mutating func prepareToPutToRealm() {
        let notEmptyPlannedExpenses = self.plannedExpenses.filter { $0.amount > 0 }
        var notEmptyDays = self.days
        
        for (index, day) in notEmptyDays.enumerated() {
            notEmptyDays[index].expenses = day.expenses.filter { $0.amount > 0 }
        }
        
        notEmptyDays = notEmptyDays.filter { $0.expenses.count > 0 }
        notEmptyDays = notEmptyDays.sorted { $0.date > $1.date }
        
        plannedExpenses = notEmptyPlannedExpenses
        days = notEmptyDays
    }
    
    mutating func removeExpenseIfExist(by unixId: Double) {
        for (dayIndex, _) in days.enumerated() {
            days[dayIndex].expenses.removeAll(where: { $0.unixId == unixId })
        }
    }
    
    func getPlannedExpensesNotYetAddedAsDoneExpenses() -> [Expense] {
        let donePlannedExpenses = plannedExpenses.filter { $0.planningStaus == .done }
        let donePlannedExpensesToBeAdded = donePlannedExpenses.filter ({ expense -> Bool in
            return allExpenses.contains(where: { $0.unixId == expense.unixId }) ? false : true
        })
        return donePlannedExpensesToBeAdded
    }
    
    func convertToRealmModel() -> PlainConvertible {
        return MonthRealm(month: self)
    }
}

class MonthRealm: Object, PlainConvertible {
    
   // typealias PlainModelType = Month
    
    @objc dynamic var unixId: String = ""
    
    @objc dynamic var date: Date = Date()
    dynamic var days = List<DayRealm>()
    
    dynamic var plannedExpensesByCategory = List<PlannedExpensesByCategoryRealm>()
    dynamic var plannedExpenses = List<ExpenseRealm>()
    
    convenience init(month: Month) {
        self.init()
        self.unixId = String(month.unixId)
        self.date = month.date
        self.days.append(objectsIn: month.days.map { DayRealm(day: $0) })
        self.plannedExpensesByCategory.append(objectsIn: month.plannedExpensesByCategory.map { PlannedExpensesByCategoryRealm(plannedExpensesByCategory: $0) })
        self.plannedExpenses.append(objectsIn: month.plannedExpenses.map { ExpenseRealm(expense: $0) })

    }
    
    func convertToPlainModel() -> RealmConvertible {
        return Month(monthRealm: self)
    }
    
    override static func primaryKey() -> String? {
        return "unixId"
    }
}



