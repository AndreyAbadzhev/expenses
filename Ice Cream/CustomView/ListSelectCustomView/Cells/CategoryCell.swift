//
//  CategoryCell.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 13.01.2020.
//  Copyright © 2020 Private. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {

    @IBOutlet weak var _backgroundView: UIView!
    @IBOutlet weak var categoryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        _backgroundView.compress()
    }
    
    func fillCell(with category: ExpenseCategory) {
        _backgroundView.layer.cornerRadius = 2
        _backgroundView.clipsToBounds = true
        _backgroundView.backgroundColor = category.categoryColorDark
        _backgroundView.addShadow()
        
        categoryLabel.text = category.rawValue
        categoryLabel.textColor = .blackDark
    }
    
}
