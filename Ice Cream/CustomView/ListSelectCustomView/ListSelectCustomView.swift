//
//  ListSelectCustomView.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 30.11.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

class ListSelectCustomView: UIView {
    
    @IBOutlet weak var darkBackgroundView: UIView!
    @IBOutlet weak var grayFrontView: UIView!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!

    var categories: [ExpenseCategory] = fullCategoriesArray
    var categorySelectedCompletion: ((ExpenseCategory) -> ())?
    
    var cellAppearDelayIncrementable: Double = 0
    
    override func awakeFromNib() {
        headerLabel.text = "Выберите категорию"
        headerLabel.textColor = .textLight
        
        var headerViewWidth: CGFloat = 16
        headerViewWidth += headerLabel.intrinsicContentSize.width
        headerView.addGradientHorizontal(mainColor: .blackLight, additionalColor: .blackDark, visibleWidth: headerViewWidth)
        
        grayFrontView.clipsToBounds = true
        grayFrontView.layer.cornerRadius = 2
        
        setupCollectionView()

        hide()
        animatedShow { self.animatedShowCollectionView() }
    }
    
    func setupCollectionView() {
        collectionView.register(CategoryCell.self)

        let cellSize = (UIScreen.main.bounds.width - 48) / 3 - 1
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        layout.itemSize = CGSize(width: cellSize, height: cellSize)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
        
        collectionView.backgroundColor = .blackDark
    }
    
    func animatedShowCollectionView() {
        for i in 0..<categories.count {
            guard let cell = collectionView.cellForItem(at: IndexPath(row: i, section: 0)) as? CategoryCell else { continue }
            cell._backgroundView.animateDecompressWithDelay(delay: cellAppearDelayIncrementable, completion: {}) //animatedShowWithDelay(delay: cellAppearDelayIncrementable)
            cellAppearDelayIncrementable += 0.025
        }
        
// почему-то не по порядку идут клетки
//        collectionView.visibleCells.forEach {
//            ($0 as? CategoryCell)?._backgroundView.animatedShowWithDelay(delay: cellAppearDelayIncrementable)
//            cellAppearDelayIncrementable += 0.05
//        }
    }
}

extension ListSelectCustomView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let category = categories.getElement(for: indexPath.row) else { return UICollectionViewCell() }
        guard let cell = collectionView.deque(CategoryCell.self, for: indexPath) else { return UICollectionViewCell() }
        cell.fillCell(with: category)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let category = categories.getElement(for: indexPath.row) else { return }
        self.categorySelectedCompletion?(category)
        self.animatedHide { self.removeFromSuperview() }
    }
}
