//
//  Label.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 28.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import UIKit

class CardLabel: UILabel {
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + 4 + 4, height: size.height + 2 + 2)
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: 2, left: 4, bottom: 2, right: 4)
        super.drawText(in: rect.inset(by: insets))
    }
}

class CardTextField: UITextField {
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + 4 + 4, height: size.height + 2 + 2)
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: 2, left: 4, bottom: 2, right: 4)
        super.drawText(in: rect.inset(by: insets))
    }
}
