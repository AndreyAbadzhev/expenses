//
//  SectionHeaderForExpenseCategoryView.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 11.01.2020.
//  Copyright © 2020 Private. All rights reserved.
//

import UIKit

class SectionHeaderForExpenseCategoryView: UIView {

    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet private weak var colorDotView: UIView!
    @IBOutlet private weak var dayLabel: UILabel!
    @IBOutlet weak var totalExpensesLabel: UILabel!

    
    func fillCell(with expenseCategory: ExpenseCategory, totalAmount: Int) {
        colorDotView.clipsToBounds = true
        colorDotView.layer.cornerRadius = 6
        colorDotView.backgroundColor = expenseCategory.categoryColorDark
        
        dayLabel.textColor = .textLight
        totalExpensesLabel.textColor = .orangeLight
        
        dayLabel.text = expenseCategory.rawValue
        totalExpensesLabel.text = "\(totalAmount) ₽"
        
        var headerViewWidth: CGFloat = 16
        headerViewWidth += 12
        headerViewWidth += 8
        headerViewWidth += dayLabel.intrinsicContentSize.width
        headerViewWidth += 8
        headerViewWidth += totalExpensesLabel.intrinsicContentSize.width
        
        headerView.addGradientHorizontal(mainColor: .blackLight, additionalColor: .blackDark, visibleWidth: headerViewWidth)
    }
}
