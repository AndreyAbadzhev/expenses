//
//  SectionHeaderView.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 12.01.2020.
//  Copyright © 2020 Private. All rights reserved.
//

import UIKit

class SectionHeaderView: UIView {

   @IBOutlet weak var headerView: UIView!

    @IBOutlet private weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!

    
    func fillCell(with firstLabelText: String, and secondLabelText: String) {
        
        firstLabel.textColor = .textLight
        secondLabel.textColor = .orangeLight
        
        firstLabel.text = firstLabelText
        secondLabel.text = secondLabelText
        
        var headerViewWidth: CGFloat = 16
        headerViewWidth += firstLabel.intrinsicContentSize.width
        headerViewWidth += 8
        headerViewWidth += secondLabel.intrinsicContentSize.width
        
        headerView.addGradientHorizontal(mainColor: .blackLight, additionalColor: .blackDark, visibleWidth: headerViewWidth)
    }

}
