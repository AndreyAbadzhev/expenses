//
//  RealmDataBase.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 07.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
import RealmSwift
class RealmDataBase: RealmManager {
    
    let realm: Realm = try! Realm()
    
    func write<T: RealmConvertible>(plainObject: T, completion: @escaping() -> ()) {
        DispatchQueue.global(qos: .userInteractive).sync {
            guard let realmObject = plainObject.convertToRealmModel() as? Object else { return }
            try! realm.write {
                realm.add(realmObject, update: .modified)
                DispatchQueue.main.async { completion() }
            }
        }
    }
    
    func obtain<T: RealmConvertible>(plainObjectType: T.Type, success: @escaping([T]) -> ()) {
        DispatchQueue.global(qos: .userInteractive).sync {
            switch true {
            case plainObjectType == Month.self:
                let realmObjects = realm.objects(MonthRealm.self)
                let plainObjects: [RealmConvertible] = realmObjects.map { $0.convertToPlainModel() }
                DispatchQueue.main.async { success(plainObjects as? [T] ?? []) }
            default: success([])
            }
        }
    }
    
    func obtainSingleMonth<T: RealmConvertible>(plainObjectType: T.Type, key: String, success: @escaping(T?) -> ()) {
        DispatchQueue.global(qos: .userInteractive).sync {
            switch true {
            case plainObjectType == Month.self:
                let realmObject = realm.object(ofType: MonthRealm.self, forPrimaryKey: key)
                let plainObjects: RealmConvertible? = realmObject?.convertToPlainModel()
                DispatchQueue.main.async { success(plainObjects as? T) }
            default: success(nil)
            }
        }
    }
    
    func delete(plainObject: RealmConvertible, completion: @escaping() -> ()) {
        DispatchQueue.global(qos: .userInteractive).sync {
            let realmObject = plainObject.convertToRealmModel()
            if let objectToDelete = realm.object(ofType: DayRealm.self, forPrimaryKey: realmObject.unixId)  {
                try! realm.write {
                    realm.delete(objectToDelete)
                    DispatchQueue.main.async { completion() }
                }
            }
        }
    }
}
