//
//  RealmConvertibleProtocol.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 07.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
import RealmSwift
protocol RealmConvertible {
    func convertToRealmModel() -> PlainConvertible
}

protocol PlainConvertible {
    var unixId: String { get }
    func convertToPlainModel() -> RealmConvertible
}

//protocol RealmConvertible {
//    associatedtype RealmModelType
//    func convertToRealmModel() -> RealmModelType
//}
//
//protocol PlainConvertible {
//    associatedtype PlainModelType
//    func convertToPlainModel() -> PlainModelType
//}
