//
//  RealmManager.swift
//  Ice Cream
//
//  Created by Андрей Абаджев on 07.12.2019.
//  Copyright © 2019 Private. All rights reserved.
//

import Foundation
protocol RealmManager {
    func write<T: RealmConvertible>(plainObject: T, completion: @escaping() -> ())
    func obtain<T: RealmConvertible>(plainObjectType: T.Type, success: @escaping([T]) -> ())
    func obtainSingleMonth<T: RealmConvertible>(plainObjectType: T.Type, key: String, success: @escaping(T?) -> ())
    func delete(plainObject: RealmConvertible, completion: @escaping() -> ())
}
